package com.example.application_recolte_v2.sqlite;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.application_recolte_v2.modele.Graine;
import com.example.application_recolte_v2.modele.Plante;
import com.example.application_recolte_v2.modele.Recolte;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.example.application_recolte_v2.module.Utils.compareTwoDateString;
import static com.example.application_recolte_v2.sqlite.DatabaseHelper.ID;
import static com.example.application_recolte_v2.sqlite.DatabaseHelper.MODIFIER_LE;
import static com.example.application_recolte_v2.sqlite.GraineTableHelper.NOMBRE_GRAINE_EXTRAITE;
import static com.example.application_recolte_v2.sqlite.GraineTableHelper.TABLE_GRAINE;
import static com.example.application_recolte_v2.sqlite.GraineTableHelper.convertCursorToGraine;
import static com.example.application_recolte_v2.sqlite.PlantationTableHelper.CODE_LOT_PLANTE;
import static com.example.application_recolte_v2.sqlite.PlantationTableHelper.TABLE_PLANTATION;
import static com.example.application_recolte_v2.sqlite.RecolteTableHelper.CODE_RECOLTE;
import static com.example.application_recolte_v2.sqlite.RecolteTableHelper.NOMBRE_FRUIT_RECOLTER;
import static com.example.application_recolte_v2.sqlite.RecolteTableHelper.NUM_RECOLTE;
import static com.example.application_recolte_v2.sqlite.RecolteTableHelper.TABLE_RECOLTE;
import static com.example.application_recolte_v2.sqlite.RecolteTableHelper.convertCursorToRecolte;

public class PlanteTableHelper {
    /*
     * +----------------------------+
     * | Table name Section Section |
     * +----------------------------+
     */
    public static final String TABLE_PLANTE = "Plante";

    /*
     * +----------------+
     * | Column Section |
     * +----------------+
     */

    public static final String NUM_PLANTE = "num_plante";
    public static final String TYPE_FECONDATION = "type_fecondation";
    public static final String CODE_PLANTE = "code_plante";

    /*
     * +----------------+
     * | Create Section |
     * +----------------+
     */

    public static final String CREATE_TABLE_PLANTE = "CREATE TABLE " + TABLE_PLANTE +
            " (" + ID + " INTEGER PRIMARY KEY," +
            CODE_LOT_PLANTE + " TEXT NOT NULL," +
            NUM_PLANTE + " TEXT NOT NULL," +
            TYPE_FECONDATION + " TEXT NOT NULL," +
            CODE_PLANTE + " TEXT NOT NULL UNIQUE," +
            MODIFIER_LE + " DATETIME," +
            " FOREIGN KEY (" + CODE_LOT_PLANTE + ") REFERENCES "+TABLE_PLANTATION+"("+CODE_LOT_PLANTE+"));";

    /*
     * +----------------+
     * | Insert Section |
     * +----------------+
     */

    public static int insertOnPlanteHelper(DatabaseHelper databaseHelper, Plante plante){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CODE_LOT_PLANTE, plante.getCode_lot_plante());
        values.put(NUM_PLANTE, plante.getNum_plante());
        values.put(TYPE_FECONDATION, plante.getType_fecondation());
        values.put(CODE_PLANTE, plante.getCode_plante());
        values.put(MODIFIER_LE, plante.getModifier_le());

        return (int) db.insert(TABLE_PLANTE, null, values);
    }

    /*
     * +----------------+
     * | Update Section |
     * +----------------+
     */

    public static int updateOnPlanteHelper(DatabaseHelper databaseHelper, Plante plante){
        int result = -1;

        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CODE_LOT_PLANTE, plante.getCode_lot_plante());
        values.put(NUM_PLANTE, plante.getNum_plante());
        values.put(TYPE_FECONDATION, plante.getType_fecondation());
        values.put(CODE_PLANTE, plante.getCode_plante());
        values.put(MODIFIER_LE, plante.getModifier_le());

        Plante plante2 = getPlanteByCodePlanteHelper(databaseHelper, plante.getCode_plante());

        if(compareTwoDateString(plante.getModifier_le(), plante2.getModifier_le())){
            result = db.update(TABLE_PLANTE, values, CODE_PLANTE +" =?", new String[]{plante.getCode_plante()});
        }

        return result;
    }

    /*
     * +--------------------+
     * | If Exist Section   |
     * +--------------------+
     */

    public static boolean rowPlanteExistByCodePlanteHelper(DatabaseHelper databaseHelper, String code){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_PLANTE + " WHERE " + CODE_PLANTE + " = '" + code +"'", null);

        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    /*
     * +----------------+
     * | GET Section    |
     * +----------------+
     */

    public static Plante getPlanteByCodePlanteHelper(DatabaseHelper databaseHelper, String code){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        Plante plante = null;

        String request = "SELECT * FROM " + TABLE_PLANTE + " WHERE " + CODE_PLANTE + " = '" + code +"'";

        Cursor cursor = db.rawQuery(request, null);

        if(cursor != null){
            if(cursor.moveToFirst()){
                plante = convertCursorToPlante(cursor);
            }
            cursor.close();
        }

        return plante;
    }

    public static List<Plante> getAllPlanteHelper(DatabaseHelper databaseHelper){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        List<Plante> listePlante = new ArrayList<>();

        String request = "SELECT * FROM " + TABLE_PLANTE;
        Cursor cursor = db.rawQuery(request, null);

        if(cursor != null){
            if(cursor.moveToFirst()){
                do {
                    listePlante.add(convertCursorToPlante(cursor));
                }while(cursor.moveToNext());
            }
            cursor.close();
        }

        return listePlante;
    }

    public static List<Graine> getAllGraineByCodePlanteHelper(DatabaseHelper databaseHelper, String code){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        List<Graine> listeGraine = new ArrayList<>();

        String request = "SELECT * FROM " + TABLE_PLANTE + " INNER JOIN " + TABLE_RECOLTE +
                " ON " + TABLE_PLANTE + "." + CODE_PLANTE + " = " + TABLE_RECOLTE + "." + CODE_PLANTE + " INNER JOIN " + TABLE_GRAINE +
                " ON " + TABLE_RECOLTE + "." + CODE_RECOLTE + " = " + TABLE_GRAINE + "." + CODE_RECOLTE +
                " WHERE " + TABLE_PLANTE + "." + CODE_PLANTE + " = '" + code +"'" +
                " ORDER BY " + TABLE_GRAINE + "." + CODE_RECOLTE + " ASC;";

        Cursor cursor = db.rawQuery(request, null);

        if(cursor != null){
            if(cursor.moveToFirst()){
                do {
                    listeGraine.add(convertCursorToGraine(cursor));
                }while(cursor.moveToNext());
            }
            cursor.close();
        }

        return listeGraine;
    }

    public static Recolte getLastRecolteFromPlanteByCodePlanteHelper(DatabaseHelper databaseHelper, String code){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        Recolte recolte = null;

        String request = "SELECT * FROM " + TABLE_PLANTE + " INNER JOIN " + TABLE_RECOLTE +
                " ON " + TABLE_PLANTE + "." + CODE_PLANTE + " = " + TABLE_RECOLTE + "." + CODE_PLANTE +
                " WHERE " + TABLE_PLANTE + "." + CODE_PLANTE + " = '" + code +"'" +
                " ORDER BY " + NUM_RECOLTE + " DESC LIMIT 1;";

        Cursor cursor = db.rawQuery(request, null);

        if(cursor != null){
            if(cursor.moveToFirst()){
                 recolte = convertCursorToRecolte(cursor);
            }
            cursor.close();
        }

        return recolte;
    }

    /*
     * +-----------------------+
     * | Count Section         |
     * +-----------------------+
     */

    public static int getNombreFruitRecolterByCodePlanteHelper(DatabaseHelper databaseHelper, String code){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        int total = 0;

        String request = "SELECT SUM(" + TABLE_RECOLTE + "." + NOMBRE_FRUIT_RECOLTER + ") as Total FROM " +
                TABLE_PLANTE + " INNER JOIN " + TABLE_RECOLTE + " ON " +
                TABLE_PLANTE + "." + CODE_PLANTE + " = " + TABLE_RECOLTE + "." + CODE_PLANTE +
                " WHERE " + TABLE_PLANTE + "." + CODE_PLANTE + " = '" + code +"'";

        Cursor cursor = db.rawQuery(request, null);

        if(cursor.moveToFirst()){
            total = cursor.getInt(cursor.getColumnIndex("Total"));
        }
        cursor.close();

        return total;
    }

    public static int getNombreGraineRecolterByCodePlanteHelper(DatabaseHelper databaseHelper, String code){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        int total = 0;

        String request = "SELECT SUM(" + TABLE_GRAINE + "." + NOMBRE_GRAINE_EXTRAITE + ") as Total FROM " +
                TABLE_PLANTE + " INNER JOIN " + TABLE_RECOLTE + " ON " +
                TABLE_PLANTE + "." + CODE_PLANTE + " = " + TABLE_RECOLTE + "." + CODE_PLANTE + " INNER JOIN " +
                TABLE_GRAINE + " ON " + TABLE_RECOLTE + "." + CODE_RECOLTE + " = " + TABLE_GRAINE + "." + CODE_RECOLTE +
                " WHERE " + TABLE_PLANTE + "." + CODE_PLANTE + " = '" + code +"'";

        Cursor cursor = db.rawQuery(request, null);

        if(cursor.moveToFirst()){
            total = cursor.getInt(cursor.getColumnIndex("Total"));
        }

        cursor.close();

        return total;
    }

    public static int getNombreGraineRecolterByCodeLotPlanteHelper(DatabaseHelper databaseHelper, String code){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        int total = 0;

        String request = "SELECT SUM(" + TABLE_GRAINE + "." + NOMBRE_GRAINE_EXTRAITE + ") as Total FROM " +
                TABLE_PLANTATION + " INNER JOIN " + TABLE_PLANTE + " ON " +
                TABLE_PLANTATION + "." + CODE_LOT_PLANTE + " = " + TABLE_PLANTE + "." + CODE_LOT_PLANTE + " INNER JOIN " + TABLE_RECOLTE +
                " ON " + TABLE_PLANTE + "." + CODE_PLANTE + " = " + TABLE_RECOLTE + "." + CODE_PLANTE + " INNER JOIN " + TABLE_GRAINE +
                " ON " + TABLE_RECOLTE + "." + CODE_RECOLTE + " = " + TABLE_GRAINE + "." + CODE_RECOLTE +
                " WHERE " + TABLE_PLANTATION + "." + CODE_LOT_PLANTE + " = '" + code +"'";

        Cursor cursor = db.rawQuery(request, null);

        if(cursor.moveToFirst()){
            total = cursor.getInt(cursor.getColumnIndex("Total"));
        }
        cursor.close();

        return total;
    }


    /*
     * +--------------------+
     * | Conversion Section |
     * +--------------------+
     */

    public static Plante convertCursorToPlante(Cursor cursor){
        if(cursor != null){
            Plante plante;

            String code_lot_plante = cursor.getString(cursor.getColumnIndex(CODE_LOT_PLANTE));
            String num_plante = cursor.getString(cursor.getColumnIndex(NUM_PLANTE));
            String type_fecondation = cursor.getString(cursor.getColumnIndex(TYPE_FECONDATION));
            String code_plante = cursor.getString(cursor.getColumnIndex(CODE_PLANTE));
            String datestr = cursor.getString(cursor.getColumnIndex(MODIFIER_LE));

            int total = 0;
            if(cursor.getColumnIndex("Total") != -1){
                total = cursor.getInt(cursor.getColumnIndex("Total"));
            }

            plante = new Plante(code_lot_plante, num_plante, type_fecondation, code_plante, datestr, total);

            return plante;
        }

        return null;
    }

    public static JSONObject convertListPlanteToJsonArray(List<Plante> listePlante){
        JSONObject jsonObject = new JSONObject();

        JSONArray jsonArray = new JSONArray();

        for(Plante plante : listePlante){
            JSONObject jsonBase = new JSONObject();
            try {
                jsonBase.put(CODE_LOT_PLANTE, plante.getCode_lot_plante());
                jsonBase.put(NUM_PLANTE, plante.getNum_plante());
                jsonBase.put(TYPE_FECONDATION, plante.getType_fecondation());
                jsonBase.put(CODE_PLANTE, plante.getCode_plante());
                jsonBase.put(MODIFIER_LE, plante.getModifier_le());

                jsonArray.put(jsonBase);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        try { jsonObject.put("data", jsonArray); } catch (JSONException e) { e.printStackTrace(); }

        return jsonObject;
    }
}
