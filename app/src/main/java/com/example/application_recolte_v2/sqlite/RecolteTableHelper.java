package com.example.application_recolte_v2.sqlite;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.application_recolte_v2.modele.Recolte;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.example.application_recolte_v2.module.Utils.compareTwoDateString;
import static com.example.application_recolte_v2.sqlite.DatabaseHelper.ID;
import static com.example.application_recolte_v2.sqlite.DatabaseHelper.MODIFIER_LE;
import static com.example.application_recolte_v2.sqlite.GraineTableHelper.TABLE_GRAINE;
import static com.example.application_recolte_v2.sqlite.PlanteTableHelper.CODE_PLANTE;
import static com.example.application_recolte_v2.sqlite.PlanteTableHelper.TABLE_PLANTE;

public class RecolteTableHelper {
    /*
     * +----------------------------+
     * | Table name Section Section |
     * +----------------------------+
     */

    public static final String TABLE_RECOLTE = "Recolte";

    /*
     * +----------------+
     * | Column Section |
     * +----------------+
     */

    public static final String CODE_RECOLTE = "code_recolte";
    public static final String NUM_RECOLTE = "num_recolte";
    public static final String NOMBRE_FRUIT_RECOLTER = "nombre_fruit_recolter";

    /*
     * +----------------+
     * | Create Section |
     * +----------------+
     */

    public static final String CREATE_TABLE_RECOLTE = "CREATE TABLE " + TABLE_RECOLTE +
            " (" + ID + " INTEGER PRIMARY KEY," +
            CODE_PLANTE + " TEXT NOT NULL," +
            CODE_RECOLTE + " TEXT NOT NULL UNIQUE," +
            NUM_RECOLTE + " INTEGER NOT NULL," +
            NOMBRE_FRUIT_RECOLTER + " INTEGER NOT NULL," +
            MODIFIER_LE + " DATETIME," +
            " FOREIGN KEY (" + CODE_PLANTE + ") REFERENCES "+TABLE_PLANTE+"("+CODE_PLANTE+"));";

    /*
     * +----------------+
     * | Insert Section |
     * +----------------+
     */

    public static int insertOnRecolteHelper(DatabaseHelper databaseHelper, Recolte recolte){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CODE_PLANTE, recolte.getCode_plante());
        values.put(CODE_RECOLTE, recolte.getCode_recolte());
        values.put(NUM_RECOLTE, recolte.getNum_recolte());
        values.put(NOMBRE_FRUIT_RECOLTER, recolte.getNombre_fruit_recolter());
        values.put(MODIFIER_LE, recolte.getModifier_le());

        return (int)db.insert(TABLE_RECOLTE, null, values);
    }

    /*
     * +----------------+
     * | Update Section |
     * +----------------+
     */

    public static int updateOnRecolteHelper(DatabaseHelper databaseHelper, Recolte recolte){
        int result = -1;

        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CODE_PLANTE, recolte.getCode_plante());
        values.put(CODE_RECOLTE, recolte.getCode_recolte());
        values.put(NUM_RECOLTE, recolte.getNum_recolte());
        values.put(NOMBRE_FRUIT_RECOLTER, recolte.getNombre_fruit_recolter());
        values.put(MODIFIER_LE, recolte.getModifier_le());

        Recolte recolte2 = getRecolteByCodeRecolteHelper(databaseHelper, recolte.getCode_recolte());

        assert recolte2 != null;
        if(compareTwoDateString(recolte.getModifier_le(), recolte2.getModifier_le())){
            result = db.update(TABLE_RECOLTE, values, CODE_RECOLTE +" =?", new String[]{recolte.getCode_recolte()});
        }

        return result;
    }

    /*
     * +----------------+
     * | Delete Section |
     * +----------------+
     */

    public static void deleteRecolteAndGraineByCodeRecolteHelper(DatabaseHelper databaseHelper, String code){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        db.delete(TABLE_RECOLTE, CODE_RECOLTE + "=?", new String[]{code});
        db.delete(TABLE_GRAINE, CODE_RECOLTE + "=?", new String[]{code});
    }

    /*
     * +--------------------+
     * | If Exist Section   |
     * +--------------------+
     */

    public static boolean rowRecolteExistByCodeRecolteHelper(DatabaseHelper databaseHelper, String code){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_RECOLTE + " WHERE " + CODE_RECOLTE + " = '" + code +"'", null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    /*
     * +----------------+
     * | GET Section    |
     * +----------------+
     */

    public static Recolte getRecolteByCodeRecolteHelper(DatabaseHelper databaseHelper, String code){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        String request = "SELECT * FROM " + TABLE_RECOLTE + " WHERE " + CODE_RECOLTE + " = '" + code +"'";

        Cursor cursor = db.rawQuery(request, null);

        if(cursor != null){
            if(cursor.moveToFirst()){
                return convertCursorToRecolte(cursor);
            }
        }

        return null;
    }

    public static List<Recolte> getAllRecolteHelper(DatabaseHelper databaseHelper){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        List<Recolte> listeRecolte = new ArrayList<>();

        String request = "SELECT * FROM " + TABLE_RECOLTE;
        Cursor cursor = db.rawQuery(request, null);

        if(cursor != null){
            if(cursor.moveToFirst()){
                do {
                    listeRecolte.add(convertCursorToRecolte(cursor));
                }while(cursor.moveToNext());
            }
            cursor.close();
        }

        return listeRecolte;
    }

    public static List<Recolte> getAllRecolteByCodePlanteHelper(DatabaseHelper databaseHelper, String code){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        List<Recolte> listeRecolte = new ArrayList<>();

        String request = "SELECT * FROM " +  TABLE_PLANTE + " INNER JOIN " + TABLE_RECOLTE +
                " ON " + TABLE_PLANTE + "." + CODE_PLANTE + " = " + TABLE_RECOLTE + "." + CODE_PLANTE +
                " WHERE " + TABLE_PLANTE + "." + CODE_PLANTE + " = '" + code + "'" +
                " ORDER BY " + TABLE_RECOLTE + "." + NUM_RECOLTE + " ASC;";

        Cursor cursor = db.rawQuery(request, null);

        if(cursor != null){
            if(cursor.moveToFirst()){
                do {
                    listeRecolte.add(convertCursorToRecolte(cursor));
                }while(cursor.moveToNext());
            }
            cursor.close();
        }

        return listeRecolte;
    }

    /*
     * +--------------------+
     * | Count Section      |
     * +--------------------+
     */

    public static int countRowRecolteByCodePlanteHelper(DatabaseHelper databaseHelper, String code){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        String request = "SELECT * FROM " + TABLE_RECOLTE + " WHERE " + CODE_PLANTE + " = '" + code +"'";
        Cursor cursor = db.rawQuery(request, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    /*
     * +--------------------+
     * | Conversion Section |
     * +--------------------+
     */

    public static Recolte convertCursorToRecolte(Cursor cursor){
        if(cursor != null){
            Recolte recolte;

            String code_plante = cursor.getString(cursor.getColumnIndex(CODE_PLANTE));
            String code_recolte = cursor.getString(cursor.getColumnIndex(CODE_RECOLTE));
            int num_recolte = cursor.getInt(cursor.getColumnIndex(NUM_RECOLTE));
            int nombre_fruit_recolter = cursor.getInt(cursor.getColumnIndex(NOMBRE_FRUIT_RECOLTER));
            String datestr = cursor.getString(cursor.getColumnIndex(MODIFIER_LE));

            recolte = new Recolte(code_plante, code_recolte, num_recolte,nombre_fruit_recolter, datestr);

            return recolte;
        }

        return null;
    }

    public static JSONObject convertListRecolteToJsonArray(List<Recolte> listeRecolte){
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        for(Recolte recolte : listeRecolte){
            JSONObject jsonBase = new JSONObject();
            try{
                jsonBase.put(CODE_PLANTE, recolte.getCode_plante());
                jsonBase.put(CODE_RECOLTE, recolte.getCode_recolte());
                jsonBase.put(NUM_RECOLTE, recolte.getNum_recolte());
                jsonBase.put(NOMBRE_FRUIT_RECOLTER, recolte.getNombre_fruit_recolter());
                jsonBase.put(MODIFIER_LE, recolte.getModifier_le());

                jsonArray.put(jsonBase);
            } catch(JSONException e){
                e.printStackTrace();
            }
        }
        try { jsonObject.put("data", jsonArray); } catch (JSONException e) { e.printStackTrace(); }

        return jsonObject;
    }
}
