package com.example.application_recolte_v2.sqlite;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.application_recolte_v2.modele.Graine;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.example.application_recolte_v2.module.Utils.compareTwoDateString;
import static com.example.application_recolte_v2.sqlite.DatabaseHelper.ID;
import static com.example.application_recolte_v2.sqlite.DatabaseHelper.MODIFIER_LE;
import static com.example.application_recolte_v2.sqlite.RecolteTableHelper.CODE_RECOLTE;
import static com.example.application_recolte_v2.sqlite.RecolteTableHelper.TABLE_RECOLTE;

public class GraineTableHelper {
    /*
     * +----------------------------+
     * | Table name Section Section |
     * +----------------------------+
     */

    public static final String TABLE_GRAINE = "Graine";

    /*
     * +----------------+
     * | Column Section |
     * +----------------+
     */

    public static final String NOMBRE_GRAINE_EXTRAITE = "nombre_graine_extraite";
    public static final String CODE_GRAINE = "code_graine";

    /*
     * +----------------+
     * | Create Section |
     * +----------------+
     */

    public static final String CREATE_TABLE_GRAINE = "CREATE TABLE " + TABLE_GRAINE +
            " (" + ID + " INTEGER PRIMARY KEY," +
            CODE_RECOLTE + " TEXT NOT NULL UNIQUE," +
            CODE_GRAINE + " TEXT NOT NULL UNIQUE," +
            NOMBRE_GRAINE_EXTRAITE + " INTEGER NOT NULL," +
            MODIFIER_LE + " DATETIME," +
            " FOREIGN KEY (" + CODE_RECOLTE + ") REFERENCES "+TABLE_RECOLTE+"("+CODE_RECOLTE+"));";

    /*
     * +----------------+
     * | Insert Section |
     * +----------------+
     */

    public static int insertOnGraineHelper(DatabaseHelper databaseHelper, Graine graine){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CODE_RECOLTE, graine.getCode_recolte());
        values.put(CODE_GRAINE, graine.getCode_graine());
        values.put(NOMBRE_GRAINE_EXTRAITE, graine.getNombre_graine_extraite());
        values.put(MODIFIER_LE, graine.getModifier_le());

        return (int)db.insert(TABLE_GRAINE, null, values);
    }

    /*
     * +----------------+
     * | Update Section |
     * +----------------+
     */

    public static int updateOnGraineHelper(DatabaseHelper databaseHelper, Graine graine){
        int result = -1;

        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CODE_RECOLTE, graine.getCode_recolte());
        values.put(CODE_GRAINE, graine.getCode_graine());
        values.put(NOMBRE_GRAINE_EXTRAITE, graine.getNombre_graine_extraite());
        values.put(MODIFIER_LE, graine.getModifier_le());

        Graine graine2 = databaseHelper.getGraineByCodeGraine(graine.getCode_graine());

        if(graine2 == null){
            Log.e("ADEL", "GRAINE UPDATE NULL");
        }

        assert graine2 != null;
        if(compareTwoDateString(graine.getModifier_le(), graine2.getModifier_le())){
            result = db.update(TABLE_GRAINE, values, CODE_GRAINE +" =?", new String[]{graine.getCode_graine()});
        }

        return result;
    }

    /*
     * +----------------+
     * | GET Section    |
     * +----------------+
     */

    public static Graine getGraineByCodeGraineHelper(DatabaseHelper databaseHelper, String code){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        String request = "SELECT * FROM " + TABLE_GRAINE + " WHERE " + CODE_GRAINE + " = '" + code +"'";

        Cursor cursor = db.rawQuery(request, null);

        if(cursor != null){
            if(cursor.moveToFirst()){
                return convertCursorToGraine(cursor);
            }
        }

        return null;
    }

    public static List<Graine> getAllGraineHelper(DatabaseHelper databaseHelper){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        List<Graine> listeGraine = new ArrayList<>();

        String request = "SELECT * FROM " + TABLE_GRAINE;
        Cursor cursor = db.rawQuery(request, null);

        if(cursor != null){
            if(cursor.moveToFirst()){
                do {
                    listeGraine.add(convertCursorToGraine(cursor));
                }while(cursor.moveToNext());
            }
            cursor.close();
        }

        return listeGraine;
    }

    /*
     * +--------------------+
     * | If Exist Section   |
     * +--------------------+
     */

    public static boolean rowGraineExistByCodeGraineHelper(DatabaseHelper databaseHelper, String code){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_GRAINE + " WHERE " + CODE_GRAINE + " = '" + code +"'", null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    /*
     * +--------------------+
     * | Conversion Section |
     * +--------------------+
     */

    public static Graine convertCursorToGraine(Cursor cursor){
        if(cursor != null){
            Graine graine;

            String code_recolte = cursor.getString(cursor.getColumnIndex(CODE_RECOLTE));
            String code_graine = cursor.getString(cursor.getColumnIndex(CODE_GRAINE));
            int nombre_graine = cursor.getInt(cursor.getColumnIndex(NOMBRE_GRAINE_EXTRAITE));
            String datestr = cursor.getString(cursor.getColumnIndex(MODIFIER_LE));

            graine = new Graine(code_recolte, code_graine, nombre_graine, datestr);

            return graine;
        }

        return null;
    }

    public static JSONObject convertListGraineToJsonArray(List<Graine> listeGraine){
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        for(Graine graine : listeGraine){
            JSONObject jsonBase = new JSONObject();
            try{
                jsonBase.put(CODE_RECOLTE, graine.getCode_recolte());
                jsonBase.put(CODE_GRAINE, graine.getCode_graine());
                jsonBase.put(NOMBRE_GRAINE_EXTRAITE, graine.getNombre_graine_extraite());
                jsonBase.put(MODIFIER_LE, graine.getModifier_le());

                jsonArray.put(jsonBase);
            }catch(JSONException e){
                e.printStackTrace();
            }
        }
        try { jsonObject.put("data", jsonArray); } catch (JSONException e) { e.printStackTrace(); }

        return jsonObject;
    }
}
