package com.example.application_recolte_v2.sqlite;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.application_recolte_v2.modele.Plantation;
import com.example.application_recolte_v2.modele.Plante;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.example.application_recolte_v2.module.Utils.compareTwoDateString;
import static com.example.application_recolte_v2.sqlite.DatabaseHelper.ID;
import static com.example.application_recolte_v2.sqlite.DatabaseHelper.MODIFIER_LE;
import static com.example.application_recolte_v2.sqlite.GraineTableHelper.NOMBRE_GRAINE_EXTRAITE;
import static com.example.application_recolte_v2.sqlite.GraineTableHelper.TABLE_GRAINE;
import static com.example.application_recolte_v2.sqlite.PlanteTableHelper.CODE_PLANTE;
import static com.example.application_recolte_v2.sqlite.PlanteTableHelper.NUM_PLANTE;
import static com.example.application_recolte_v2.sqlite.PlanteTableHelper.TABLE_PLANTE;
import static com.example.application_recolte_v2.sqlite.PlanteTableHelper.TYPE_FECONDATION;
import static com.example.application_recolte_v2.sqlite.PlanteTableHelper.convertCursorToPlante;
import static com.example.application_recolte_v2.sqlite.RecolteTableHelper.CODE_RECOLTE;
import static com.example.application_recolte_v2.sqlite.RecolteTableHelper.TABLE_RECOLTE;

public class PlantationTableHelper {

    public static final int PLANTATION_EMPTY = 510;

    /*
     * +----------------------------+
     * | Table name Section Section |
     * +----------------------------+
     */

    public static final String TABLE_PLANTATION = "Plantation";

    /*
     * +----------------+
     * | Column Section |
     * +----------------+
     */

    public static final String CODE_ACCESSION = "code_accession";
    public static final String CODE_LOT_PLANTE = "code_lot_plante";
    public static final String LIEU = "lieu";
    public static final String PARCELLE = "parcelle";
    public static final String NOM_LOT = "nom_lot";
    public static final String NOMBRE_PLANTE = "nombre_plante";
    public static final String LOT_ORIGINE = "lot_origine";
    public static final String ESPECE = "espece";
    public static final String GRAINE_OBJECTIF = "graine_objectif";
    public static final String REMARQUE = "remarque";

    /*
     * +----------------+
     * | Create Section |
     * +----------------+
     */

    public static final String CREATE_TABLE_PLANTATION = "CREATE TABLE " + TABLE_PLANTATION +
            " (" + ID + " INTEGER PRIMARY KEY," +
            CODE_ACCESSION + " TEXT NOT NULL," +
            CODE_LOT_PLANTE + " TEXT NOT NULL UNIQUE," +
            LIEU + " TEXT NOT NULL," +
            PARCELLE + " TEXT NOT NULL," +
            NOM_LOT + " TEXT NOT NULL," +
            NOMBRE_PLANTE + " INTEGER NOT NULL," +
            LOT_ORIGINE + " TEXT NOT NULL," +
            ESPECE + " TEXT NOT NULL," +
            GRAINE_OBJECTIF + " INTEGER NOT NULL," +
            REMARQUE + " TEXT NOT NULL," +
            MODIFIER_LE + " DATETIME)";

    /*
     * +----------------+
     * | Insert Section |
     * +----------------+
     */

    public static int insertOnPlantationHelper(DatabaseHelper databaseHelper, Plantation plantation){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CODE_ACCESSION, plantation.getCode_accession());
        values.put(CODE_LOT_PLANTE, plantation.getCode_lot_plante());
        values.put(LIEU, plantation.getLieu());
        values.put(PARCELLE, plantation.getParcelle());
        values.put(NOM_LOT, plantation.getNom_lot());
        values.put(NOMBRE_PLANTE, plantation.getNombre_plante());
        values.put(LOT_ORIGINE, plantation.getLot_origine());
        values.put(ESPECE, plantation.getEspece());
        values.put(GRAINE_OBJECTIF, plantation.getGraine_objectif());
        values.put(REMARQUE, plantation.getRemarque());
        values.put(MODIFIER_LE, plantation.getModifier_le());

        return (int)db.insert(TABLE_PLANTATION, null, values);
    }

    /*
     * +----------------+
     * | Update Section |
     * +----------------+
     */

    public static int updateOnPlantationHelper(DatabaseHelper databaseHelper, Plantation plantation){
        int result = -1;

        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CODE_ACCESSION, plantation.getCode_accession());
        values.put(CODE_LOT_PLANTE, plantation.getCode_lot_plante());
        values.put(LIEU, plantation.getLieu());
        values.put(PARCELLE, plantation.getParcelle());
        values.put(NOM_LOT, plantation.getNom_lot());
        values.put(NOMBRE_PLANTE, plantation.getNombre_plante());
        values.put(LOT_ORIGINE, plantation.getLot_origine());
        values.put(ESPECE, plantation.getEspece());
        values.put(GRAINE_OBJECTIF, plantation.getGraine_objectif());
        values.put(REMARQUE, plantation.getRemarque());
        values.put(MODIFIER_LE, plantation.getModifier_le());

        Plantation plantation2 = getPlantationByCodeLotPlanteHelper(databaseHelper, plantation.getCode_lot_plante());

        assert plantation2 != null;
        if(compareTwoDateString(plantation.getModifier_le(), plantation2.getModifier_le())){
            result = db.update(TABLE_PLANTATION, values, CODE_LOT_PLANTE +" =?", new String[]{plantation.getCode_lot_plante()});
        }

        return result;
    }

    /*
     * +--------------------+
     * | If Exist Section   |
     * +--------------------+
     */

    public static boolean rowPlantationExistByCodeLotPlanteHelper(DatabaseHelper databaseHelper, String code){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_PLANTATION + " WHERE " + CODE_LOT_PLANTE + " = '" + code +"'", null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    /*
     * +----------------+
     * | GET Section    |
     * +----------------+
     */

    public static Plantation getPlantationByCodeLotPlanteHelper(DatabaseHelper databaseHelper, String code){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        Plantation plantation = null;

        String request = "SELECT * FROM " + TABLE_PLANTATION + " WHERE " + CODE_LOT_PLANTE + " = '" + code +"'";

        Cursor cursor = db.rawQuery(request, null);

        if(cursor != null){
            if(cursor.moveToFirst()){
                plantation = convertCursorToPlantation(cursor);
            }
            cursor.close();
        }

        return plantation;
    }

    public static List<Plante> getAllPlanteFromPlantationByCodeLotPlanteHelper(DatabaseHelper databaseHelper, String code){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        List<Plante> listePlante = new ArrayList<>();

        String request = "SELECT * FROM " + TABLE_PLANTATION + " INNER JOIN " + TABLE_PLANTE +
                " ON " + TABLE_PLANTATION + "." + CODE_LOT_PLANTE + " = " + TABLE_PLANTE + "." + CODE_LOT_PLANTE +
                " WHERE " + TABLE_PLANTATION + "." + CODE_LOT_PLANTE + " = '" + code +"'" +
                " ORDER BY " + TABLE_PLANTE + "." + CODE_PLANTE;

        Cursor cursor = db.rawQuery(request, null);

        if(cursor != null){
            if(cursor.moveToFirst()){
                do {
                    listePlante.add(convertCursorToPlante(cursor));
                } while(cursor.moveToNext());
            }
            cursor.close();
        }

        return listePlante;
    }

    public static List<Plante> getAllPlanteForRecyclerViewByCodeLotPlanteHelper(DatabaseHelper databaseHelper, String code){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        List<Plante> listePlante = new ArrayList<>();

        String request = "SELECT " + TABLE_PLANTE + "." + NUM_PLANTE + ", " + TABLE_PLANTE + "." + TYPE_FECONDATION + ", " + TABLE_PLANTE + "." + CODE_PLANTE + ", " +
                TABLE_PLANTE + "." + CODE_LOT_PLANTE + ", " + TABLE_PLANTE + "." + MODIFIER_LE +", COALESCE(SUM(" + TABLE_GRAINE + "." + NOMBRE_GRAINE_EXTRAITE + "), 0) AS Total FROM " + TABLE_PLANTATION + " LEFT OUTER JOIN " + TABLE_PLANTE +
                " ON " + TABLE_PLANTATION + "." + CODE_LOT_PLANTE + " = " + TABLE_PLANTE + "." + CODE_LOT_PLANTE + " LEFT OUTER JOIN " + TABLE_RECOLTE + " ON " +
                TABLE_PLANTE + "." + CODE_PLANTE + " = " + TABLE_RECOLTE + "." + CODE_PLANTE + " LEFT OUTER JOIN " + TABLE_GRAINE + " ON " +
                TABLE_RECOLTE + "." + CODE_RECOLTE + " = " + TABLE_GRAINE + "." + CODE_RECOLTE +
                " WHERE " + TABLE_PLANTATION + "." + CODE_LOT_PLANTE + " = '" + code + "'" +
                " GROUP BY " + TABLE_PLANTE + "." + CODE_PLANTE;

        Cursor cursor = db.rawQuery(request, null);

        if(cursor != null){
            if(cursor.moveToFirst()){
                do {
                    listePlante.add(convertCursorToPlante(cursor));
                }while(cursor.moveToNext());
            }
            cursor.close();
        }

        return listePlante;
    }

    public static List<Plante> getAllPlanteForRecyclerViewByCodeLotPlanteExceptCurrentPlanteHelper(DatabaseHelper databaseHelper, String code, String code_plante){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        List<Plante> listePlante = new ArrayList<>();

        String request = "SELECT " + TABLE_PLANTE + "." + NUM_PLANTE + ", " + TABLE_PLANTE + "." + TYPE_FECONDATION + ", " + TABLE_PLANTE + "." + CODE_PLANTE + ", " +
                TABLE_PLANTE + "." + CODE_LOT_PLANTE + ", " + TABLE_PLANTE + "." + MODIFIER_LE +", COALESCE(SUM(" + TABLE_GRAINE + "." + NOMBRE_GRAINE_EXTRAITE + "), 0) AS Total FROM " + TABLE_PLANTATION + " LEFT OUTER JOIN " + TABLE_PLANTE +
                " ON " + TABLE_PLANTATION + "." + CODE_LOT_PLANTE + " = " + TABLE_PLANTE + "." + CODE_LOT_PLANTE + " LEFT OUTER JOIN " + TABLE_RECOLTE + " ON " +
                TABLE_PLANTE + "." + CODE_PLANTE + " = " + TABLE_RECOLTE + "." + CODE_PLANTE + " LEFT OUTER JOIN " + TABLE_GRAINE + " ON " +
                TABLE_RECOLTE + "." + CODE_RECOLTE + " = " + TABLE_GRAINE + "." + CODE_RECOLTE +
                " WHERE " + TABLE_PLANTATION + "." + CODE_LOT_PLANTE + " = '" + code + "'" +
                " AND " + TABLE_PLANTE + "." + CODE_PLANTE + " != '" + code_plante + "'" +
                " GROUP BY " + TABLE_PLANTE + "." + CODE_PLANTE;

        Cursor cursor = db.rawQuery(request, null);

        if(cursor != null){
            if(cursor.moveToFirst()){
                do {
                    listePlante.add(convertCursorToPlante(cursor));
                }while(cursor.moveToNext());
            }
            cursor.close();
        }

        return listePlante;
    }

    /*
     * +----------------+
     * | Count Section  |
     * +----------------+
     */

    public static int countRowPlanteByCodeLotPlanteHelper(DatabaseHelper databaseHelper, String code){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        int total = 0;

        String request = "SELECT COUNT(" + TABLE_PLANTE + "." + CODE_PLANTE + ") as Total FROM " +
                TABLE_PLANTATION + " INNER JOIN " + TABLE_PLANTE + " ON " +
                TABLE_PLANTATION + "." + CODE_LOT_PLANTE + " = " + TABLE_PLANTE + "." + CODE_LOT_PLANTE +
                " WHERE " + TABLE_PLANTATION + "." + CODE_LOT_PLANTE + " = '" + code +"'";

        Cursor cursor = db.rawQuery(request, null);

        if(cursor.moveToFirst()){
            total = cursor.getInt(cursor.getColumnIndex("Total"));
        }
        cursor.close();

        return total;
    }

    /*
     * +-----------------+
     * | If Table Empty  |
     * +-----------------+
     */

    public static boolean isTablePlantationEmptyHelper(DatabaseHelper databaseHelper){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        int total = 0;

        String request = "SELECT COUNT(*) FROM " + TABLE_PLANTATION;

        Cursor cursor = db.rawQuery(request, null);

        if(cursor.moveToFirst()){
            total = cursor.getInt(0);
        }
        cursor.close();

        return total == 0;
    }



    /*
     * +--------------------+
     * | Conversion Section |
     * +--------------------+
     */

    public static Plantation convertCursorToPlantation(Cursor cursor) {
        if(cursor != null){
            Plantation plantation;

            String code_accession = cursor.getString(cursor.getColumnIndex(CODE_ACCESSION));
            String code_lot_plante = cursor.getString(cursor.getColumnIndex(CODE_LOT_PLANTE));
            String lieu = cursor.getString(cursor.getColumnIndex(LIEU));
            String parcelle = cursor.getString(cursor.getColumnIndex(PARCELLE));
            String nom_lot = cursor.getString(cursor.getColumnIndex(NOM_LOT));
            int nombre_plante = cursor.getInt(cursor.getColumnIndex(NOMBRE_PLANTE));
            String lot_origine = cursor.getString(cursor.getColumnIndex(LOT_ORIGINE));
            String espece = cursor.getString(cursor.getColumnIndex(ESPECE));
            int graine_objectif = cursor.getInt(cursor.getColumnIndex(GRAINE_OBJECTIF));
            String remarque = cursor.getString(cursor.getColumnIndex(REMARQUE));
            String modifier_le = cursor.getString(cursor.getColumnIndex(MODIFIER_LE));

            plantation = new Plantation(code_accession, code_lot_plante, lieu, parcelle, nom_lot, nombre_plante, lot_origine, espece, graine_objectif, remarque, modifier_le);

            return plantation;
        }

        return null;
    }

    public static JSONObject convertListPlantationToJsonObject(List<Plantation> listePlantation) {
        JSONObject jsonObject = new JSONObject();

        JSONArray jsonArray = new JSONArray();

        for(Plantation base : listePlantation){
            JSONObject jsonBase = new JSONObject();
            try {
                jsonBase.put(CODE_ACCESSION, base.getCode_accession());
                jsonBase.put(CODE_LOT_PLANTE, base.getCode_lot_plante());
                jsonBase.put(LIEU, base.getLieu());
                jsonBase.put(PARCELLE, base.getParcelle());
                jsonBase.put(NOM_LOT, base.getNom_lot());
                jsonBase.put(NOMBRE_PLANTE, base.getNombre_plante());
                jsonBase.put(LOT_ORIGINE, base.getLot_origine());
                jsonBase.put(ESPECE, base.getEspece());
                jsonBase.put(GRAINE_OBJECTIF, base.getGraine_objectif());
                jsonBase.put(REMARQUE, base.getRemarque());
                jsonArray.put(jsonBase);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        try { jsonObject.put("data", jsonArray); } catch (JSONException e) { e.printStackTrace(); }

        return jsonObject;
    }
}
