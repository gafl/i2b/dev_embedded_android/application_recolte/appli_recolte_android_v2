package com.example.application_recolte_v2.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.application_recolte_v2.modele.Graine;
import com.example.application_recolte_v2.modele.Plantation;
import com.example.application_recolte_v2.modele.Plante;
import com.example.application_recolte_v2.modele.Recolte;

import java.util.List;

import static com.example.application_recolte_v2.sqlite.GraineTableHelper.CREATE_TABLE_GRAINE;
import static com.example.application_recolte_v2.sqlite.GraineTableHelper.TABLE_GRAINE;
import static com.example.application_recolte_v2.sqlite.GraineTableHelper.getAllGraineHelper;
import static com.example.application_recolte_v2.sqlite.GraineTableHelper.getGraineByCodeGraineHelper;
import static com.example.application_recolte_v2.sqlite.GraineTableHelper.insertOnGraineHelper;
import static com.example.application_recolte_v2.sqlite.GraineTableHelper.rowGraineExistByCodeGraineHelper;
import static com.example.application_recolte_v2.sqlite.GraineTableHelper.updateOnGraineHelper;
import static com.example.application_recolte_v2.sqlite.PlantationTableHelper.CREATE_TABLE_PLANTATION;
import static com.example.application_recolte_v2.sqlite.PlantationTableHelper.TABLE_PLANTATION;
import static com.example.application_recolte_v2.sqlite.PlantationTableHelper.countRowPlanteByCodeLotPlanteHelper;
import static com.example.application_recolte_v2.sqlite.PlantationTableHelper.getAllPlanteForRecyclerViewByCodeLotPlanteExceptCurrentPlanteHelper;
import static com.example.application_recolte_v2.sqlite.PlantationTableHelper.getAllPlanteForRecyclerViewByCodeLotPlanteHelper;
import static com.example.application_recolte_v2.sqlite.PlantationTableHelper.getAllPlanteFromPlantationByCodeLotPlanteHelper;
import static com.example.application_recolte_v2.sqlite.PlantationTableHelper.getPlantationByCodeLotPlanteHelper;
import static com.example.application_recolte_v2.sqlite.PlantationTableHelper.insertOnPlantationHelper;
import static com.example.application_recolte_v2.sqlite.PlantationTableHelper.isTablePlantationEmptyHelper;
import static com.example.application_recolte_v2.sqlite.PlantationTableHelper.rowPlantationExistByCodeLotPlanteHelper;
import static com.example.application_recolte_v2.sqlite.PlantationTableHelper.updateOnPlantationHelper;
import static com.example.application_recolte_v2.sqlite.PlanteTableHelper.CREATE_TABLE_PLANTE;
import static com.example.application_recolte_v2.sqlite.PlanteTableHelper.TABLE_PLANTE;
import static com.example.application_recolte_v2.sqlite.PlanteTableHelper.getAllGraineByCodePlanteHelper;
import static com.example.application_recolte_v2.sqlite.PlanteTableHelper.getAllPlanteHelper;
import static com.example.application_recolte_v2.sqlite.PlanteTableHelper.getLastRecolteFromPlanteByCodePlanteHelper;
import static com.example.application_recolte_v2.sqlite.PlanteTableHelper.getNombreFruitRecolterByCodePlanteHelper;
import static com.example.application_recolte_v2.sqlite.PlanteTableHelper.getNombreGraineRecolterByCodeLotPlanteHelper;
import static com.example.application_recolte_v2.sqlite.PlanteTableHelper.getNombreGraineRecolterByCodePlanteHelper;
import static com.example.application_recolte_v2.sqlite.PlanteTableHelper.getPlanteByCodePlanteHelper;
import static com.example.application_recolte_v2.sqlite.PlanteTableHelper.insertOnPlanteHelper;
import static com.example.application_recolte_v2.sqlite.PlanteTableHelper.rowPlanteExistByCodePlanteHelper;
import static com.example.application_recolte_v2.sqlite.PlanteTableHelper.updateOnPlanteHelper;
import static com.example.application_recolte_v2.sqlite.RecolteTableHelper.CREATE_TABLE_RECOLTE;
import static com.example.application_recolte_v2.sqlite.RecolteTableHelper.TABLE_RECOLTE;
import static com.example.application_recolte_v2.sqlite.RecolteTableHelper.countRowRecolteByCodePlanteHelper;
import static com.example.application_recolte_v2.sqlite.RecolteTableHelper.deleteRecolteAndGraineByCodeRecolteHelper;
import static com.example.application_recolte_v2.sqlite.RecolteTableHelper.getAllRecolteByCodePlanteHelper;
import static com.example.application_recolte_v2.sqlite.RecolteTableHelper.getAllRecolteHelper;
import static com.example.application_recolte_v2.sqlite.RecolteTableHelper.getRecolteByCodeRecolteHelper;
import static com.example.application_recolte_v2.sqlite.RecolteTableHelper.insertOnRecolteHelper;
import static com.example.application_recolte_v2.sqlite.RecolteTableHelper.rowRecolteExistByCodeRecolteHelper;
import static com.example.application_recolte_v2.sqlite.RecolteTableHelper.updateOnRecolteHelper;

@SuppressWarnings({"UnusedReturnValue", "unused"})
public class DatabaseHelper extends SQLiteOpenHelper  {
    /*
     * +----------------------------+
     * | Data base name & version   |
     * +----------------------------+
     */

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "appliRecolte";

    /*
     * +----------------------------+
     * |       Common column        |
     * +----------------------------+
     */

    public static final String ID = "_id";
    public static final String MODIFIER_LE = "modifier_le";

    /*
     * +----------------------------+
     * |     Data base builder      |
     * +----------------------------+
     */

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_PLANTATION);
        db.execSQL(CREATE_TABLE_PLANTE);
        db.execSQL(CREATE_TABLE_RECOLTE);
        db.execSQL(CREATE_TABLE_GRAINE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLANTATION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLANTE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECOLTE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GRAINE);
        onCreate(db);
    }

    /*
     * +----------------+
     * | Insert Section |
     * +----------------+
     */

    public int insertOnPlantation(Plantation plantation) { return insertOnPlantationHelper(this, plantation); }
    public int insertOnPlante(Plante plante) { return insertOnPlanteHelper(this, plante); }
    public int insertOnRecolte(Recolte recolte) { return insertOnRecolteHelper(this, recolte);}
    public int insertOnGraine(Graine graine) { return insertOnGraineHelper(this, graine); }

    /*
     * +----------------+
     * | Update Section |
     * +----------------+
     */

    public int updateOnPlantation(Plantation plantation) { return updateOnPlantationHelper(this, plantation); }
    public int updateOnPlante(Plante plante) { return updateOnPlanteHelper(this, plante); }
    public int updateOnRecolte(Recolte recolte) { return updateOnRecolteHelper(this, recolte); }
    public int updateOnGraine(Graine graine) { return updateOnGraineHelper(this, graine); }

    /*
     * +----------------+
     * | Delete Section |
     * +----------------+
     */

    public void deleteRecolteAndGraineByCodeRecolte(String code) { deleteRecolteAndGraineByCodeRecolteHelper(this, code); }

    /*
     * +--------------------+
     * | If Exist Section   |
     * +--------------------+
     */

    public boolean rowPlantationExistByCodeLotPlante(String code) { return rowPlantationExistByCodeLotPlanteHelper(this, code); }
    public boolean rowPlanteExistByCodePlante(String code) { return rowPlanteExistByCodePlanteHelper(this, code);}
    public boolean rowRecolteExistByCodeRecolte(String code) { return rowRecolteExistByCodeRecolteHelper(this, code); }
    public boolean rowGraineExistByCodeGraine(String code) { return rowGraineExistByCodeGraineHelper(this, code); }

    /*
     * +----------------+
     * | GET Section    |
     * +----------------+
     */

    // Simple get
    public Plantation getPlantationByCodeLotPlante(String code) { return getPlantationByCodeLotPlanteHelper(this, code); }
    public Plante getPlanteByCodePlante(String code) { return getPlanteByCodePlanteHelper(this, code); }
    public Recolte getRecolteByCodeRecolte(String code) { return getRecolteByCodeRecolteHelper(this, code); }
    public Graine getGraineByCodeGraine(String code) { return getGraineByCodeGraineHelper(this, code); }
    public Recolte getLastRecolteFromPlanteByCodePlante(String code) { return getLastRecolteFromPlanteByCodePlanteHelper(this, code);}

    // multiple get
    public List<Plante> getAllPlanteFromPlantationByCodeLotPlante(String code) { return getAllPlanteFromPlantationByCodeLotPlanteHelper(this, code);}
    public List<Plante> getAllPlante() { return getAllPlanteHelper(this); }
    public List<Plante> getAllPlanteForRecyclerViewByCodeLotPlante(String code) { return getAllPlanteForRecyclerViewByCodeLotPlanteHelper(this, code);}
    public List<Plante> getAllPlanteForRecyclerViewByCodeLotPlanteExceptCurrentPlante(String code, String code_plante) { return getAllPlanteForRecyclerViewByCodeLotPlanteExceptCurrentPlanteHelper(this, code, code_plante);}
    public List<Recolte> getAllRecolte() { return getAllRecolteHelper(this);}
    public List<Recolte> getAllRecolteByCodePlante(String code) { return getAllRecolteByCodePlanteHelper(this, code);}
    public List<Graine> getAllGraine() { return getAllGraineHelper(this); }
    public List<Graine> getAllGraineByCodePlante(String code) { return getAllGraineByCodePlanteHelper(this, code); }

    /*
     * +-----------------------+
     * | Count & Sum Section   |
     * +-----------------------+
     */

    // Count
    public int countRowRecolteByCodePlante(String code){ return countRowRecolteByCodePlanteHelper(this, code); }
    public int countRowPlanteByCodeLotPlante(String code){ return countRowPlanteByCodeLotPlanteHelper(this, code); }

    // Sum
    public int getNombreFruitRecolterByCodePlante(String code) { return getNombreFruitRecolterByCodePlanteHelper(this, code);}
    public int getNombreGraineRecolterByCodePlante(String code) { return getNombreGraineRecolterByCodePlanteHelper(this, code);}
    public int getNombreGraineRecolterByCodeLotPlante(String code) { return getNombreGraineRecolterByCodeLotPlanteHelper(this, code); }

    /*
     * +-----------------+
     * | If Table Empty  |
     * +-----------------+
     */

    public boolean isTablePlantationEmpty() { return isTablePlantationEmptyHelper(this); }

}
