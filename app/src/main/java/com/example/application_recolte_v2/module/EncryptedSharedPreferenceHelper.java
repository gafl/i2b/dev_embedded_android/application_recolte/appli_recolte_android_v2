package com.example.application_recolte_v2.module;

import android.content.Context;
import android.content.SharedPreferences;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.util.Log;

import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKey;

import java.io.IOException;
import java.security.GeneralSecurityException;

import static androidx.security.crypto.MasterKey.DEFAULT_MASTER_KEY_ALIAS;

public class EncryptedSharedPreferenceHelper {

    private static final String SECRET_NAME = "SECRET_SHARED";

    public static SharedPreferences getEncryptedSharedPreference(Context context){
        KeyGenParameterSpec spec = new KeyGenParameterSpec.Builder(
                DEFAULT_MASTER_KEY_ALIAS,
                KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                .setKeySize(256)
                .build();

        MasterKey masterKey = null;
        try {
            masterKey = new MasterKey.Builder(context)
                    .setKeyGenParameterSpec(spec)
                    .build();

            return EncryptedSharedPreferences.create(
                    context,
                    "SECRET_NAME",
                    masterKey, // masterKey created above
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM);
        } catch (GeneralSecurityException | IOException e) {
            Log.e("ERROR", "Error on getting encrypted shared preferences", e);
        }

        return null;
    }

}
