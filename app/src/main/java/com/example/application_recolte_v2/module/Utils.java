package com.example.application_recolte_v2.module;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.BlendMode;
import android.graphics.BlendModeColorFilter;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceManager;

import com.example.application_recolte_v2.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class Utils {

    public static String createCopyAndReturnRealPath(@NonNull Context context, @NonNull Uri uri) {
        final ContentResolver contentResolver = context.getContentResolver();
        if (contentResolver == null)
            return null;

        // Create file path inside app's data dir
        String filePath = context.getApplicationInfo().dataDir + File.separator
                + System.currentTimeMillis();

        File file = new File(filePath);
        try {
            InputStream inputStream = contentResolver.openInputStream(uri);
            if (inputStream == null)
                return null;

            OutputStream outputStream = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while ((len = inputStream.read(buf)) > 0)
                outputStream.write(buf, 0, len);

            outputStream.close();
            inputStream.close();
        } catch (IOException ignore) {
            return null;
        }

        return file.getAbsolutePath();
    }

    public static void setAppLanguage(Activity activity) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
        String langue = sharedPreferences.getString("langage_pref", "fr");
        Locale locale = new Locale(langue);
        Locale.setDefault(locale);
        Resources resources = activity.getResources();
        Configuration config = resources.getConfiguration();
        config.setLocale(locale);
        resources.updateConfiguration(config, resources.getDisplayMetrics());
    }

    public static void setAppLanguage(Activity activity, String languageCode) {
        Locale locale = new Locale(languageCode);
        Locale.setDefault(locale);
        Resources resources = activity.getResources();
        Configuration config = resources.getConfiguration();
        config.setLocale(locale);
        resources.updateConfiguration(config, resources.getDisplayMetrics());
    }

    public static String getSettingLanguage(Activity activity){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
        return sharedPreferences.getString("langage_pref", "fr");
    }

    public static void restartActivity(Activity activity) {
        Intent intent = activity.getIntent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        activity.finish();
        activity.startActivity(intent);
        activity.overridePendingTransition(0, 0);
    }

    public static void hideKeyboard(Activity activity){
        try {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            // Do nothing
        }
    }

    public static boolean compareTwoDateString(String new_dateStr, String old_dateStr){
        OffsetDateTime new_date = convertStringDateToOffsetDate(new_dateStr);
        OffsetDateTime old_date = convertStringDateToOffsetDate(old_dateStr);

        return new_date.compareTo(old_date) > 0;
    }

    public static String beautifyStringDate(String dateStr){
        String jour = dateStr.substring(8,10);
        String mois = dateStr.substring(5,7);
        String annee = dateStr.substring(0,4);
        String dateFr = jour + "/" + mois + "/" + annee;
        String time = dateStr.substring(11,19);
        return dateFr + " - " + time;
    }

    public static OffsetDateTime convertStringDateToOffsetDate(String dateStr){
        DateTimeFormatter f = DateTimeFormatter.ofPattern("yyyy-MM-dd['T'][ ][HH:mm:ss][HH:mm][.][SSSSSS][SSSSS][SSSS][SSS][XXX][XX][X]").withZone(ZoneId.of("Europe/Paris"));
        return ZonedDateTime.parse(dateStr, f).toOffsetDateTime();
    }

    public static void enableButtonWithBackgroundColor(Context context, Button button, boolean enable){
        if(enable) {
            button.setEnabled(true);
            button.getBackground().clearColorFilter();
        } else {
            button.setEnabled(false);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                button.getBackground().setColorFilter(new BlendModeColorFilter(context.getResources().getColor(R.color.disable, context.getTheme()), BlendMode.SRC_ATOP));
            } else {
                button.getBackground().setColorFilter(context.getResources().getColor(R.color.disable, context.getTheme()), PorterDuff.Mode.SRC_ATOP);
            }
        }
    }
}
