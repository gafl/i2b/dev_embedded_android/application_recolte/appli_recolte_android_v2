package com.example.application_recolte_v2.module;

import android.content.Context;
import android.os.Looper;
import android.util.Log;

import com.example.application_recolte_v2.R;
import com.example.application_recolte_v2.modele.ActionCallback;
import com.example.application_recolte_v2.modele.Plantation;
import com.example.application_recolte_v2.modele.Plante;
import com.example.application_recolte_v2.modele.Recolte;
import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.printer.PrinterStatus;
import com.zebra.sdk.printer.SGD;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException;
import com.zebra.sdk.printer.ZebraPrinterLinkOs;

public class ZebraPrint
{
    private final ActionCallback requestCallBack;
    private Connection connection;
    private ZebraPrinter printer;
    private String addressMac;
    private final Context context;
    private Plantation plantation;
    private Plante plante;
    private Recolte recolte;
    private boolean isFinal;

    public ZebraPrint(ActionCallback requestCallBack, Context context){
        this.requestCallBack = requestCallBack;
        this.context = context;
    }

    public void print(String addressMac, Plante plante, Plantation plantation, Recolte recolte, boolean isFinal){
        this.addressMac = addressMac;
        this.plante = plante;
        this.plantation = plantation;
        this.recolte = recolte;
        this.isFinal = isFinal;

        new Thread(() -> {
            Looper.prepare();
            doConnection();
            Looper.loop();
            Looper.myLooper().quit();
        }).start();
    }

    private void doConnection(){
        try{
            printer = connect();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (printer != null) {
            printQrCode();
        }
    }

    private void printQrCode() {
        try {
            ZebraPrinterLinkOs linkOsPrinter = ZebraPrinterFactory.createLinkOsPrinter(printer);

            PrinterStatus printerStatus = (linkOsPrinter != null) ? linkOsPrinter.getCurrentStatus() : printer.getCurrentStatus();
            if (printerStatus.isReadyToPrint) {
                byte[] configLabel;
                if(!isFinal){
                    configLabel = getZplStringToPrint().getBytes();
                } else{
                    configLabel = getZplStringFinalToPrint().getBytes();
                }
                connection.write(configLabel);
            }

//            Thread.sleep(3000);
//            if (connection instanceof BluetoothConnection) {
//                String friendlyName = ((BluetoothConnection) connection).getFriendlyName();
//                Thread.sleep(500);
//            }
        } catch (ConnectionException e) {
            requestCallBack.action(1, e.getMessage());
        } finally {
            disconnect();
        }
    }

    private String getZplStringToPrint() {
        int lwidth = 250;
        int llenght = 500;

        return String.format(
             "^XA" +
                     "^CF0,25" + "\r\n" +
                     "^FO10,25^FD" + plantation.getCode_accession() + "^FS" + "\r\n" +
                     "^CFA,20" + "\r\n" +
                     "^FO11,60^FDL: ^FS" + "\r\n" +
                     "^FO95,60^FDP: ^FS" + "\r\n" +
                     "^FO11,88^FDNum Plante:^FS" + "\r\n" +
                     "^FO11,115^FDNb Fruits:^FS" + "\r\n" +
                     "^FO11,140^FDDate:^FS" + "\r\n" +
                     "^FO11,166^FDNum Recolte: ^FS" + "\r\n" +
                     "^CFA,20" + "\r\n" +
                     "^FO250,55^BQ,2,5^FDMM,A" + plante.getCode_plante() + "^FS" + "\r\n" +
                     "^CF0,19" + "\r\n" +
                     "^FO35,60^FD" + plantation.getLieu() + "^FS" + "\r\n" +
                     "^FO120,60^FD" + plantation.getParcelle() + "^FS" + "\r\n" +
                     "^FO145,88^FD" + plante.getNum_plante() +"^FS" + "\r\n" +
                     "^FO135,115^FD" + recolte.getNombre_fruit_recolter() + "^FS" + "\r\n" +
                     "^FO70,140^FD" + recolte.getModifier_le().substring(0, 10) + "^FS" + "\r\n" +
                     "^FO157,166^FD" + recolte.getNum_recolte() +"^FS" + "\r\n" +
                     "^XZ", lwidth, llenght
        );
    }

    private String getZplStringFinalToPrint() {
        int lwidth = 250;
        int llenght = 500;

        return String.format(
                "^XA" +
                        "^FO0,15" + "\r\n" +
                        "^GB500,3,3^FS" + "\r\n" +
                        "^FO0,75" + "\r\n" +
                        "^GB500,3,3^FS" + "\r\n" +
                        "^FO0,159" + "\r\n" +
                        "^GB500,3,3^FS" + "\r\n" +
                        "^CF0,16" + "\r\n" +
                        "^FO10,26^FD" + plantation.getEspece() + "^FS" + "\r\n" +
                        "^FO10,50^FD" + plantation.getNom_lot() + "^FS" + "\r\n" +
                        "^CF0,20" + "\r\n" +
                        "^FO230,26^FD" + plantation.getCode_accession() + "^FS" + "\r\n" +
                        "^CFA,14" + "\r\n" +
                        "^FO10,85^FDLieu: ^FS" + "\r\n" +
                        "^FO10,110^FDParcelle: ^FS" + "\r\n" +
                        "^FO10,135^FDNum: ^FS" + "\r\n" +
                        "^FO160,85^FDAnnee: ^FS" + "\r\n" +
                        "^FO160,135^FDType:^FS" + "\r\n" +
                        "^FO10,169^FDOrigine: ^FS" + "\r\n" +
                        "^FO315,77^BQ,2,3^FDMM,A" + plante.getCode_plante() + "^FS" + "\r\n" +
                        "^CF0,20" + "\r\n" +
                        "^FO70,85^FD" + plantation.getLieu() + "^FS" + "\r\n" +
                        "^FO118,110^FD" + plantation.getParcelle() +"^FS" + "\r\n" +
                        "^FO60,135^FD" + plante.getNum_plante() +"^FS" + "\r\n" +
                        "^FO233,85^FD" + recolte.getModifier_le().substring(0, 4) +"^FS" + "\r\n" +
                        "^FO227,135^FD" + plante.getType_fecondation() + "^FS" + "\r\n" +
                        "^FO112,168^FD" + plantation.getLot_origine() + "^FS" + "\r\n" +
                        "^XZ", lwidth, llenght
        );

    }

    @SuppressWarnings("RedundantThrows")
    private ZebraPrinter connect() throws InterruptedException {
        connection = null;
        try {
            connection = new BluetoothConnection(addressMac);
            SettingsHelper.saveBluetoothAddress(context, addressMac);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return null;
        }

        try {
            connection.open();
            //Thread.sleep(1000);
        } catch (ConnectionException e) {
            //Thread.sleep(1000);
            try {
                connection.close();
            } catch (ConnectionException connectionException) {
                connectionException.printStackTrace();
            }
            SettingsHelper.saveBluetoothAddress(context, "none");
            requestCallBack.action(ActionCallback.ERROR, context.getResources().getString(R.string.no_zebra_printer));
//            Intent intent = new Intent(context, BluetoothScannerActivity.class);
//            ((Activity) context).startActivityForResult(intent, RecolteFragment.LAUNCH_BLUETOOTH_SCAN_ACTIVITY);
        }

        ZebraPrinter printer = null;

        if (connection.isConnected()) {
            try {
                printer = ZebraPrinterFactory.getInstance(connection);
                //Thread.sleep(1000);
                //noinspection unused
                String pl = SGD.GET("device.languages", connection);
                //Thread.sleep(1000);
            } catch (ConnectionException | ZebraPrinterLanguageUnknownException e) {
                printer = null;
                //Thread.sleep(1000);
                disconnect();
            }
        }

        return printer;
    }

    private void disconnect() {
        try {
            if (connection != null) {
                connection.close();
            }
            requestCallBack.action(ActionCallback.SUCCESS, context.getResources().getString(R.string.fin_impression));
        } catch (ConnectionException e) {
            requestCallBack.action(ActionCallback.ERROR, context.getResources().getString(R.string.error_deconnexion_zebra));
            Log.e("ZebraError", e.getMessage());
        }
    }
}
