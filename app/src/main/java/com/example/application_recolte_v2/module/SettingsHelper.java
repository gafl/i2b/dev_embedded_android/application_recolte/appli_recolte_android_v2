package com.example.application_recolte_v2.module;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.preference.PreferenceManager;

import java.util.Objects;

import static com.example.application_recolte_v2.module.EncryptedSharedPreferenceHelper.getEncryptedSharedPreference;

public class SettingsHelper {
    private static final String PREFS_NAME = "APP_RECOLTE";
    private static final String MAC_ADDRESSE_KEY = "ZEBRA_DEMO_BLUETOOTH_ADDRESS";
    private static final String TOKEN_KEY = "TOKEN_KEY";
    private static final String NEW_DATA_LOCAL = "NEW_DATA_LOCAL";
    private static final String USERNAME_KEY = "USERNAME_KEY";
    private static final String PASSWORD_KEY = "PASSWORD_KEY";


    public static Boolean isDatabaseChange(Context context){
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        return settings.getBoolean(NEW_DATA_LOCAL, false);
    }

    public static void databaseHasChanged(Context context){
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(NEW_DATA_LOCAL, true);
        editor.apply();

    }

    public static void databaseSynchronised(Context context){
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(NEW_DATA_LOCAL, false);
        editor.apply();
    }

    public static String getBluetoothAddress(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        return settings.getString(MAC_ADDRESSE_KEY, "none");
    }

    public static void saveBluetoothAddress(Context context, String address) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(MAC_ADDRESSE_KEY, address);
        editor.apply();
    }

    public static String getIpFromPreference(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString("ip_parametre", "none");
    }

    public static String getPortFromPreference(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString("port_parametre", "none");
    }

    /*
     * +----------------------------+
     * | Encrypted Token storage    |
     * +----------------------------+
     */

    public static boolean isConnected(Context context){
        String token = Objects.requireNonNull(getEncryptedSharedPreference(context)).getString(TOKEN_KEY, "none");
        return !token.equals("none");
    }

    public static String getTokenStored(Context context) {
        return Objects.requireNonNull(getEncryptedSharedPreference(context)).getString(TOKEN_KEY, "none");
    }

    public static void saveToken(Context context, String token) {
        Objects.requireNonNull(getEncryptedSharedPreference(context)).edit()
                .putString(TOKEN_KEY, token)
                .apply();
    }

    public static void deleteToken(Context context) {
        Objects.requireNonNull(getEncryptedSharedPreference(context)).edit()
                .putString(TOKEN_KEY, "none")
                .apply();
    }

    /*
     * +------------------------------------------+
     * | Encrypted Username & Password storage    |
     * +------------------------------------------+
     */

    public static void storeEncryptedUsernameAndPassword(Context context, String username, String password){
        Objects.requireNonNull(getEncryptedSharedPreference(context)).edit()
                .putString(USERNAME_KEY, username)
                .putString(PASSWORD_KEY, password)
                .apply();
    }

    public static void deleteEncryptedUsernameAndPassword(Context context){
        Objects.requireNonNull(getEncryptedSharedPreference(context)).edit()
                .putString(USERNAME_KEY, "")
                .putString(PASSWORD_KEY, "")
                .apply();
    }

    public static String getUsername(Context context){
        return Objects.requireNonNull(getEncryptedSharedPreference(context)).getString(USERNAME_KEY, "");
    }

    public static String getPassword(Context context){
        return Objects.requireNonNull(getEncryptedSharedPreference(context)).getString(PASSWORD_KEY, "");
    }


}
