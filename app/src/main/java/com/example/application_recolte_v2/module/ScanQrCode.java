package com.example.application_recolte_v2.module;

import android.app.Activity;
import android.content.Context;

import com.example.application_recolte_v2.R;
import com.google.zxing.integration.android.IntentIntegrator;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ScanQrCode {

    public static final int REQUEST_CODE_XZING_SCANNER = 49374;

    public ScanQrCode(){
    }

    public static void initScan(Activity activity){
        IntentIntegrator integrator = new IntentIntegrator(activity);
        integrator.setOrientationLocked(true);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt(activity.getResources().getString(R.string.scan_message));
        integrator.initiateScan();
    }


    // Permet de valider un QR CODE
    public static class QrCodeValidation {

        public QrCodeValidation() {}

        public static boolean verification(String qrcode, Context context, int min_length, int max_length) {

            qrcode = qrcode.toUpperCase();

            List<String> listeUnite = Arrays.asList(context.getResources().getStringArray(R.array.unite));
            List<String> listeCodeCollection = Arrays.asList(context.getResources().getStringArray(R.array.codeCollection));
            List<String> listeCodeType = Arrays.asList(context.getResources().getStringArray(R.array.codeType));

            // Test d'exception sur le QR CODE
            if(qrcode.length() < min_length || qrcode.length() > max_length) { return false; }

            // Vérifie si le nom d'unité du qrcode correspond avec la liste fournis
            boolean isValid = verificationUnite(listeUnite, qrcode);

            // Vérifie si le code de collection du qrcode correspond avec la liste fournis (si le test précédent a fonctionné)
            if(isValid) { verificationCollection(listeCodeCollection, qrcode); }

            // Vérifie si le code du type du qrcode correspond avec la liste fournis (si le test précédent a fonctionné)
            if(isValid) { verificationType(listeCodeType, qrcode); }

            return isValid;
        }

        private static boolean verificationUnite(List<String> listeUnite, String qrcode) {
            if(listeUnite == null || listeUnite.isEmpty()) { return false; }

            for(String unite : listeUnite) {
                Pattern pattern = Pattern.compile(unite);
                Matcher matcher = pattern.matcher(qrcode);
                if(matcher.find()) {
                    return true;
                }
            }

            return false;
        }

        @SuppressWarnings("UnusedReturnValue")
        private static boolean verificationCollection(List<String> listeCodeCollection, String qrcode) {

            if(listeCodeCollection == null || listeCodeCollection.isEmpty()) {
                return false;
            }

            for(String code : listeCodeCollection) {
                Pattern pattern = Pattern.compile(code);
                Matcher matcher = pattern.matcher(qrcode);
                if(matcher.find()) {
                    return true;
                }
            }

            return false;
        }

        @SuppressWarnings("UnusedReturnValue")
        private static boolean verificationType(List<String> listeCodeType, String qrcode)  {

            if(listeCodeType == null || listeCodeType.isEmpty()) {
                return false;
            }

            for(String code : listeCodeType) {
                Pattern pattern = Pattern.compile(code);
                Matcher matcher = pattern.matcher(qrcode);
                if(matcher.find()) {
                    return true;
                }
            }

            return false;
        }

    }
}

