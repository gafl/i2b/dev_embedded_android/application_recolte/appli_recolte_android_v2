package com.example.application_recolte_v2.module;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import com.example.application_recolte_v2.R;
import com.example.application_recolte_v2.modele.Plantation;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.example.application_recolte_v2.module.ScanQrCode.QrCodeValidation.verification;
import static com.example.application_recolte_v2.module.SweetAlert.errorAlert;

public class ReadXLS {

    private String errorMessage;
    private final Context context;
    private final String pathXLS;

    public ReadXLS(String path, Context context){
        this.pathXLS = path;
        this.context = context;
    }

    public List<Plantation> readXlsAndReturnListPlantation() throws IOException {

        List<Plantation> listePlantation = new ArrayList<>();

        // Define the xls file to open
        FileInputStream file = new FileInputStream(new File(pathXLS));

        // open the xls file with workbook
        HSSFWorkbook myWorkBook = new HSSFWorkbook(file);

        // Get the first sheet from workbook
        HSSFSheet sheet = myWorkBook.getSheetAt(0);

        // Get the number of row max
        int row = sheet.getLastRowNum();

        // Define some strings
        String code_accession;
        String code_lot_plante;
        String lieu;
        String parcelle;
        String nom_lot;
        String nombre_plante;
        String lot_origine;
        String espece;
        String graine_objectif;
        String remarque;

        // from cell 1 to last
        for(int i = 1; i <= row; i++) {
            code_accession = sheet.getRow(i).getCell(0).toString();
            code_lot_plante = sheet.getRow(i).getCell(1).toString();
            lieu = sheet.getRow(i).getCell(2).toString();
            parcelle = sheet.getRow(i).getCell(3).toString();
            nom_lot = sheet.getRow(i).getCell(4).toString();
            nombre_plante = sheet.getRow(i).getCell(5).toString();
            lot_origine = sheet.getRow(i).getCell(6).toString();
            espece = sheet.getRow(i).getCell(7).toString();
            graine_objectif = sheet.getRow(i).getCell(8).toString();
            remarque = sheet.getRow(i).getCell(9).toString();

            if(nombre_plante.indexOf('.') != -1)
                nombre_plante = nombre_plante.substring(0, nombre_plante.indexOf('.'));

            if(graine_objectif.indexOf('.') != -1)
                graine_objectif = graine_objectif.substring(0, graine_objectif.indexOf('.'));

            if(verificationLineData(i+1, code_accession, code_lot_plante, lieu, parcelle, nom_lot, nombre_plante, lot_origine, espece, graine_objectif, remarque)){
                Plantation plantation = new Plantation(code_accession, code_lot_plante, lieu, parcelle, nom_lot, Integer.parseInt(nombre_plante), lot_origine, espece, Integer.parseInt(graine_objectif), remarque);
                listePlantation.add(plantation);
            } else {
                errorAlert(errorMessage, context);
                return null;
            }
        }

        return listePlantation;
    }

//    public List<Plantation> readXlsAndReturnListPlantation() throws IOException, BiffException, NumberFormatException {
//        List<Plantation> listePlantation = new ArrayList<>();
//
//        File file = new File(pathXLS);
//        Workbook workbook = Workbook.getWorkbook(file);
//
//        Sheet sheet = workbook.getSheet(0);
//        int row = sheet.getRows();
//
//        String code_accession;
//        String code_lot_plante;
//        String lieu;
//        String parcelle;
//        String nom_lot;
//        String nombre_plante;
//        String lot_origine;
//        String espece;
//        String graine_objectif;
//        String remarque;
//
//        for(int i = 1; i <= (row - 1); i++) {
//            code_accession = sheet.getCell(0, i).getContents();
//            code_lot_plante = sheet.getCell(1, i).getContents();
//            lieu = sheet.getCell(2, i).getContents();
//            parcelle = sheet.getCell(3, i).getContents();
//            nom_lot = sheet.getCell(4, i).getContents();
//            nombre_plante = sheet.getCell(5, i).getContents();
//            lot_origine = sheet.getCell(6, i).getContents();
//            espece = sheet.getCell(7, i).getContents();
//            graine_objectif = sheet.getCell(8, i).getContents();
//            remarque = sheet.getCell(9, i).getContents();
//
//            if(verificationLineData(i+1, code_accession, code_lot_plante, lieu, parcelle, nom_lot, nombre_plante, lot_origine, espece, graine_objectif, remarque)){
//                Plantation plantation = new Plantation(code_accession, code_lot_plante, lieu, parcelle, nom_lot, Integer.parseInt(nombre_plante), lot_origine, espece, Integer.parseInt(graine_objectif), remarque);
//                listePlantation.add(plantation);
//            } else {
//                errorAlert(errorMessage, context);
//                return null;
//            }
//        }
//
//        return listePlantation;
//    }

    private boolean verificationLineData(int row, String accession, String codeLotPlante, String mLieu, String mParcelle, String nomLot, String nombrePlante, String lotOrigine, String mEspecen, String graineObject, String mRemarque) {
        errorMessage = "";

        if(accession.length() < 1 || accession.length() > 100)
            errorMessage = context.getString(R.string.accession_non_conforme) + " " + row + ",A -> " + accession;

        if(!verification(codeLotPlante, context, 16, 16) && errorMessage.length() == 0)
            errorMessage = context.getString(R.string.code_lot_plante_label) + " " + codeLotPlante +  " " + context.getString(R.string.non_conforme_cellule_label) + " " + row + ",B";

        if((mLieu.length() < 1 || mLieu.length() > 2) && errorMessage.length() == 0)
            errorMessage = context.getString(R.string.lieu_non_conforme) + " " + row + ",C";

        if(mParcelle.length() != 3 && errorMessage.length() == 0)
            errorMessage = context.getString(R.string.parcelle_non_conforme) + " " + row + ",D";

        if((nomLot.length() > 500) && errorMessage.length() == 0)
            errorMessage = context.getString(R.string.nom_lot_non_conforme) + " " + row + ",E";

        if(nombrePlante.length() != 1 && errorMessage.length() == 0){
            errorMessage = context.getString(R.string.nombre_plante_length_trop_grand) + " " + row + ",F -> " + nombrePlante;
        } else if(nombrePlante.length() == 1 && errorMessage.length() == 0){
            try{
                Integer.parseInt(nombrePlante);
            } catch(NumberFormatException e){
                errorMessage = context.getString(R.string.nombre_plante_not_integer) + " " + row + ",F";
            }
        }

        if((lotOrigine.length() > 500) && errorMessage.length() == 0)
            errorMessage = context.getString(R.string.lot_origine_non_conforme) + " " + row + ",G";

        if((mEspecen.length() > 500) && errorMessage.length() == 0)
            errorMessage = context.getString(R.string.espece_non_conforme) + " " + row + ",H";

        if(graineObject.length() > 5 && errorMessage.length() == 0){
            errorMessage = context.getString(R.string.objectif_graine_trop_character) + " " + row + ",F";
        } else if(nombrePlante.length() == 1 && errorMessage.length() == 0){
            try{
                Integer.parseInt(graineObject);
            } catch(NumberFormatException e){
                errorMessage = context.getString(R.string.objectif_graine_not_integer) + " " + row + ",I";
            }
        }

        if((mRemarque.length() > 1000) && errorMessage.length() == 0)
            errorMessage = context.getString(R.string.remarque_non_conforme) + " " + row + ",J";


        return errorMessage.length() == 0;
    }
}
