package com.example.application_recolte_v2.module;

import android.content.Context;
import android.view.View;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.application_recolte_v2.module.SettingsHelper;

import static com.example.application_recolte_v2.module.SettingsHelper.isDatabaseChange;

public class StatusBarManager {
    public StatusBarManager() {}

    public static void testIfDataNeedToBeSynchronize(ConstraintLayout status_bar, Context context){
        if(SettingsHelper.isDatabaseChange(context)){
            status_bar.setVisibility(View.VISIBLE);
        } else {
            status_bar.setVisibility(View.INVISIBLE);
        }
    }
}
