package com.example.application_recolte_v2.module;

import android.content.Context;

import com.example.application_recolte_v2.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class SweetAlert {

    public static void successAlert(String titre, String message, Context context){
        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(titre)
                .setContentText(message)
                .show();
    }

    public static void successAlert(String message, Context context){
        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(context.getResources().getString(R.string.succes))
                .setContentText(message)
                .show();
    }

    public static void errorAlert(String titre, String message, Context context){
        new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(titre)
                .setContentText(message)
                .show();
    }

    public static void errorAlert(String message, Context context){
        new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(context.getResources().getString(R.string.error))
                .setContentText(message)
                .show();
    }

    public static void choiceAlert(String title, String message, Context context, CallbackSweetAlert callbackSweetAlert, int request){
        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText(context.getResources().getString(R.string.oui))
                .setConfirmClickListener(sDialog -> {
                    sDialog.dismissWithAnimation();
                    callbackSweetAlert.onActionSweetAlert(true, request);
                })
                .setCancelButton(context.getResources().getString(R.string.non), sDialog -> {
                    sDialog.dismissWithAnimation();
                    callbackSweetAlert.onActionSweetAlert(false, request);
                })
                .show();
    }

    public interface CallbackSweetAlert{
        void onActionSweetAlert(boolean bool, int request);
    }
}
