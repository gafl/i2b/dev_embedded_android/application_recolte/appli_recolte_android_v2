package com.example.application_recolte_v2;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.application_recolte_v2.activity.FragmentContainer;
import com.example.application_recolte_v2.activity.LoginActivity;
import com.example.application_recolte_v2.activity.ParametreActivity;
import com.example.application_recolte_v2.activity.PlanteAddActivity;
import com.example.application_recolte_v2.modele.ActionCallback;
import com.example.application_recolte_v2.modele.Plantation;
import com.example.application_recolte_v2.modele.Plante;
import com.example.application_recolte_v2.module.ReadXLS;
import com.example.application_recolte_v2.module.ScanQrCode;
import com.example.application_recolte_v2.module.StatusBarManager;
import com.example.application_recolte_v2.module.SweetAlert;
import com.example.application_recolte_v2.sqlite.DatabaseHelper;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.model.MediaFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.example.application_recolte_v2.module.PermissionChecker.requestCameraPermission;
import static com.example.application_recolte_v2.module.ScanQrCode.REQUEST_CODE_XZING_SCANNER;
import static com.example.application_recolte_v2.module.SettingsHelper.databaseSynchronised;
import static com.example.application_recolte_v2.module.SettingsHelper.isConnected;
import static com.example.application_recolte_v2.module.SweetAlert.choiceAlert;
import static com.example.application_recolte_v2.module.SweetAlert.errorAlert;
import static com.example.application_recolte_v2.module.SweetAlert.successAlert;
import static com.example.application_recolte_v2.module.Utils.createCopyAndReturnRealPath;
import static com.example.application_recolte_v2.module.Utils.enableButtonWithBackgroundColor;
import static com.example.application_recolte_v2.module.Utils.restartActivity;
import static com.example.application_recolte_v2.module.Utils.setAppLanguage;
import static com.example.application_recolte_v2.request.RequestGraine.getGraineFromServerToLocale;
import static com.example.application_recolte_v2.request.RequestGraine.sendGraineFromLocaleToServer;
import static com.example.application_recolte_v2.request.RequestPlantation.getPlantationFromServeurToLocale;
import static com.example.application_recolte_v2.request.RequestPlantation.sendPlantationsToServer;
import static com.example.application_recolte_v2.request.RequestPlante.getPlanteFromServerToLocale;
import static com.example.application_recolte_v2.request.RequestPlante.sendPlanteFromLocaleToServer;
import static com.example.application_recolte_v2.request.RequestRecolte.getRecolteFromServerToLocale;
import static com.example.application_recolte_v2.request.RequestRecolte.sendRecolteFromLocaleToServer;
import static com.example.application_recolte_v2.request.RequestUser.disconnectFromServeur;
import static com.example.application_recolte_v2.request.RequestUtil.NOT_CONNECTED;
import static com.example.application_recolte_v2.request.RequestUtil.WRONG_SERVER_SETTINGS;
import static com.example.application_recolte_v2.request.RequestUtil.testIfAllIsGoodBeforeConnection;

public class MainActivity extends AppCompatActivity implements ActionCallback, SweetAlert.CallbackSweetAlert {

    private ActivityResultLauncher<Intent> activityResultFilePicker, activityResultLogin, activityResultParametre;
    public SweetAlertDialog pDialog;
    public ConstraintLayout status_bar_data_sync;
    private Menu menu;
    private Context context;
    private ActionCallback actionCallback;
    private boolean prepareMenuAlreadyCalled;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Toolbar settings
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Initialization
        ImageView imageQrCode = findViewById(R.id.imageQrCode);
        Button buttonScanQrCode = findViewById(R.id.button_scanner);
        Button buttonValider = findViewById(R.id.button_valider_home);
        Button buttonSynchronize = findViewById(R.id.button_synchronize);
        EditText editText_qrcode = findViewById(R.id.editText_qrcode);
        status_bar_data_sync = findViewById(R.id.include_status_bar);
        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        context = this;
        actionCallback = this;
        TextView versionNumber = findViewById(R.id.text_version_main);

        versionNumber.setText(versionNumber.getText() + " " + getString(R.string.version_number));

        // change color icon image
        switch (getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK) {
            case Configuration.UI_MODE_NIGHT_YES:
                imageQrCode.setImageResource(R.drawable.qr_code_white_512_px);
                break;
            case Configuration.UI_MODE_NIGHT_NO:
                imageQrCode.setImageResource(R.drawable.qr_code_black_512_px);
                break;
        }

        enableButtonWithBackgroundColor(this, buttonValider, false);

        StatusBarManager.testIfDataNeedToBeSynchronize(status_bar_data_sync, this);

        // Listener button to launch scan qr code camera
        buttonScanQrCode.setOnClickListener(view -> {
            if(databaseHelper.isTablePlantationEmpty()){
                errorAlert(getString(R.string.database_empty), this);
            } else {
                requestCameraPermission(this, this);
            }
        });

        editText_qrcode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }

            @Override
            public void afterTextChanged(Editable s) {
                String text = editText_qrcode.getText().toString();
                boolean test = text.length() >= 16 && text.length() <= 18;
                enableButtonWithBackgroundColor(context, buttonValider, test);
            }
        });

        buttonValider.setOnClickListener(view -> {
            String qrcode = editText_qrcode.getText().toString().toUpperCase();
            editText_qrcode.setText("");
            readQRCODE(qrcode);
        });

        buttonSynchronize.setOnClickListener(view -> {
            launchLoadingDialog("Synchronisation plantations ...");
            new Thread(() -> getPlantationFromServeurToLocale(this, this)).start();
        });

        // Activity result from file picker
        activityResultFilePicker = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        assert result.getData() != null;
                        ArrayList<MediaFile> mediaFiles = result.getData().getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);
                        if(!mediaFiles.isEmpty()){
                            // Get real path from file
                            String path = createCopyAndReturnRealPath(this, mediaFiles.get(0).getUri());

                            Log.e("ADEL", "Chemin: " + path);

                            // Read file and validate it
                            ReadXLS readXLS = new ReadXLS(path, this);
                            try {
                                List<Plantation> listePlantation = readXLS.readXlsAndReturnListPlantation();

                                if(listePlantation != null){
                                    launchLoadingDialog(getResources().getString(R.string.envoie_fichier_xls_en_cours));
                                    new Thread(() -> sendPlantationsToServer(this, this, listePlantation)).start();
                                }
                            } catch (IOException e) {
                                errorAlert(getResources().getString(R.string.mauvais_chemin_fichier) + ": " + path, this);
                                Log.e("ADEL", e.getMessage());
                            }
                        }
                    }
                });

        // Activity result from login activity
        activityResultLogin = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        new Thread(() -> {
                            this.runOnUiThread(() -> successAlert(getResources().getString(R.string.connexion_succès), context));
                        }).start();
                    }
                    new Thread(() -> {
                        prepareMenuAlreadyCalled = false;
                        invalidateOptionsMenu();
                    }).start();
                });

        // Activity result from parametre activity
        activityResultParametre = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == SUCCESS) {
                        setAppLanguage(this);
                        restartActivity(this);
                    }
                });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);

        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if(item.getItemId() == R.id.action_parametre){
            activityResultParametre.launch(new Intent(this, ParametreActivity.class));

            overridePendingTransition(0, 0);
        }
        if(item.getItemId() == R.id.action_deconnexion){
            if(isConnected(this)){
                launchLoadingDialog(getResources().getString(R.string.deconnexion_en_cours));
                new Thread(() -> disconnectFromServeur(this, this)).start();
            } else {
                launchLoadingDialog("Verification du serveur");
                new Thread(() -> {
                    if(testIfAllIsGoodBeforeConnection(this, this)){
                        this.runOnUiThread(() -> {
                            pDialog.dismiss();
                            launchLoginActivityForResult();
                        });
                    }
                }).start();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = this.menu.findItem(R.id.action_deconnexion);

        if(!prepareMenuAlreadyCalled)
        {
            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = this.getTheme();
            theme.resolveAttribute(R.attr.colorPrimaryVariant, typedValue, true);
            @ColorInt int color = typedValue.data;

            if (isConnected(this)) {
                item.setTitle(R.string.se_deconnecter);
                SpannableString spanString = new SpannableString(item.getTitle().toString());
                spanString.setSpan(new ForegroundColorSpan(color),0,spanString.length(),0);
                item.setTitle(spanString);
            } else {
                item.setTitle(R.string.se_connecter);
                SpannableString spanString = new SpannableString(item.getTitle().toString());

                spanString.setSpan(new ForegroundColorSpan(color),0,spanString.length(),0);
                item.setTitle(spanString);
            }
            prepareMenuAlreadyCalled = true;
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && data != null && requestCode == REQUEST_CODE_XZING_SCANNER){
            String qrcode = result.getContents();
            qrcode = qrcode.toUpperCase();
            readQRCODE(qrcode);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void action(int action, String message) {
        switch(action) {
            case PERMISSION_CAMERA_GRANTED:
                ScanQrCode.initScan(MainActivity.this);
                break;
            case GET_PLANTATION:
                pDialog.setTitleText("Synchronisation plantations ...");
                new Thread(() -> getPlantationFromServeurToLocale(context, actionCallback)).start();
                break;
            case GET_PLANTE:
                pDialog.setTitleText("Synchronisation plantes ...");
                new Thread(() -> getPlanteFromServerToLocale(this, this)).start();
                break;
            case SEND_PLANTE:
                new Thread(() -> sendPlanteFromLocaleToServer(this, this)).start();
                break;
            case GET_RECOLTE:
                pDialog.setTitleText("Synchronisation recoltes ...");
                new Thread(() -> getRecolteFromServerToLocale(this, this)).start();
                break;
            case SEND_RECOLTE:
                new Thread(() -> sendRecolteFromLocaleToServer(this, this)).start();
                break;
            case GET_GRAINE:
                pDialog.setTitleText("Synchronisation extractions ...");
                new Thread(() -> getGraineFromServerToLocale(this, this)).start();
                break;
            case SEND_GRAINE:
                new Thread(() -> sendGraineFromLocaleToServer(this, this)).start();
                break;
            case ERROR:
                this.runOnUiThread(() -> {
                    pDialog.dismiss();
                    errorAlert(message, this);
                });
                break;
            case SUCCESS:
                this.runOnUiThread(() -> {
                    pDialog.dismiss();
                    databaseSynchronised(context);
                    StatusBarManager.testIfDataNeedToBeSynchronize(status_bar_data_sync, this);
                    successAlert(message, context);
                });
                break;
            case SUCCESS_DISCONNECTION:
                this.runOnUiThread(() -> {
                    pDialog.dismiss();
                    successAlert(message, this);
                    new Thread(() -> {
                        prepareMenuAlreadyCalled = false;
                        invalidateOptionsMenu();
                    }).start();
                });
                break;
            case NOT_CONNECTED:
                this.runOnUiThread(() -> {
                    pDialog.dismiss();
                    this.runOnUiThread(() -> choiceAlert(getResources().getString(R.string.probleme), message, this, this, NOT_CONNECTED));
                });
                break;
            case WRONG_SERVER_SETTINGS:
                this.runOnUiThread(() -> {
                    pDialog.dismiss();
                    choiceAlert(getResources().getString(R.string.probleme), message, this, this, WRONG_SERVER_SETTINGS);
                });
                break;
        }
    }

    @Override
    public void onActionSweetAlert(boolean bool, int request) {
        if(bool && request == NOT_CONNECTED){
            launchLoginActivityForResult();
        }

        if(bool && request == WRONG_SERVER_SETTINGS){
            activityResultParametre.launch(new Intent(this, ParametreActivity.class));
            overridePendingTransition(0, 0);
        }
    }

    private void launchLoginActivityForResult(){
        activityResultLogin.launch(new Intent(MainActivity.this, LoginActivity.class));
        overridePendingTransition(0, 0);
    }

    private void launchLoadingDialog(String title) {
        pDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(title);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    private void readQRCODE(String qrcode){
        boolean isGoodFormat = ScanQrCode.QrCodeValidation.verification(qrcode, this, 16, 18);
        DatabaseHelper databaseHelper = new DatabaseHelper(this);

        if(qrcode.length() == 16 && isGoodFormat){
            Plantation plantation = databaseHelper.getPlantationByCodeLotPlante(qrcode);
            if(plantation == null){
                errorAlert(qrcode + " " + getResources().getString(R.string.existe_pas) + " !", this);
            } else {
                Intent intent = new Intent(MainActivity.this, PlanteAddActivity.class);
                intent.putExtra("plantation", plantation);
                startActivity(intent);
            }
        } else if(qrcode.length() == 18 && isGoodFormat){
            Plante plante = databaseHelper.getPlanteByCodePlante(qrcode);
            if(plante == null){
                errorAlert(qrcode + " " + getResources().getString(R.string.existe_pas) + " !", this);
            } else {
                Intent intent = new Intent(MainActivity.this, FragmentContainer.class);
                intent.putExtra("plante", plante);
                startActivity(intent);
            }
        } else {
            errorAlert(qrcode + " " + getResources().getString(R.string.est_pas_valide), this);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        StatusBarManager.testIfDataNeedToBeSynchronize(status_bar_data_sync, this);
    }
}