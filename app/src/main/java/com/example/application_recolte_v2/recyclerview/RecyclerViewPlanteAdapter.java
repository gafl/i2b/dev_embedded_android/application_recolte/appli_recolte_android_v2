package com.example.application_recolte_v2.recyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.application_recolte_v2.R;
import com.example.application_recolte_v2.modele.Plante;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class RecyclerViewPlanteAdapter extends RecyclerView.Adapter<RecyclerViewPlanteAdapter.RecyclerViewPlanteHolder> {

    private final List<Plante> listePlante;
    private final Context myContext;
    private final OnItemListener mOnItemListener;
    private final int[] backgroundColors = {
            R.color.item_1,
            R.color.item_2,
            R.color.item_3,
            R.color.item_4,
            R.color.item_5,
            R.color.item_6
    };


    public RecyclerViewPlanteAdapter(List<Plante> listePlante, Context myContext, OnItemListener mOnItemListener) {
        this.listePlante = listePlante;
        this.myContext = myContext;
        this.mOnItemListener = mOnItemListener;
    }

    public static class RecyclerViewPlanteHolder extends RecyclerView.ViewHolder {

        public final TextView textCodePlante;
        public final ImageView image_rond;

        public RecyclerViewPlanteHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            textCodePlante = itemView.findViewById(R.id.code_plante_autre_plante);
            image_rond = itemView.findViewById(R.id.round_circle_autre_plante);
        }
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerViewPlanteHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recyclerview_autre_plante, parent, false);
        return new RecyclerViewPlanteAdapter.RecyclerViewPlanteHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerViewPlanteAdapter.RecyclerViewPlanteHolder holder, int position) {
        int index = position % backgroundColors.length;
        //int color = ContextCompat.getColor(myContext, backgroundColors[index]);

        holder.image_rond.setColorFilter(ContextCompat.getColor(myContext, backgroundColors[index]));

        Plante currentPlante = listePlante.get(position);

        holder.textCodePlante.setText(currentPlante.getCode_plante());

        // On item click callback
        holder.itemView.setOnClickListener(v -> mOnItemListener.onItemClick(position));
    }

    @Override
    public int getItemCount() { return listePlante.size(); }


}
