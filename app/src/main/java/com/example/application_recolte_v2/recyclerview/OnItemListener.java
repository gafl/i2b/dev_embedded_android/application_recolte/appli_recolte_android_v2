package com.example.application_recolte_v2.recyclerview;

public interface OnItemListener {
    void onItemClick(int position);
    void onLongItemClick(int position);
}
