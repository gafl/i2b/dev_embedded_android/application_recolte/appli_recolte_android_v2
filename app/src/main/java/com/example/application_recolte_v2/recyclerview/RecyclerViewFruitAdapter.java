package com.example.application_recolte_v2.recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.application_recolte_v2.R;
import com.example.application_recolte_v2.modele.Recolte;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import static com.example.application_recolte_v2.module.Utils.beautifyStringDate;

public class RecyclerViewFruitAdapter extends RecyclerView.Adapter<RecyclerViewFruitAdapter.RecyclerViewFruitHolder>{

    private final List<Recolte> listeRecolte;
    private final OnItemListener mOnItemListener;

    public RecyclerViewFruitAdapter(List<Recolte> listeRecolte, OnItemListener mOnItemListener) {
        this.listeRecolte = listeRecolte;
        this.mOnItemListener = mOnItemListener;
    }

    public static class RecyclerViewFruitHolder extends RecyclerView.ViewHolder {

        public final TextView textNumeroRecolte;
        public final TextView textFruitRecolter;
        public final TextView textDate;

        public RecyclerViewFruitHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            textNumeroRecolte = itemView.findViewById(R.id.text_num_recolte);
            textFruitRecolter = itemView.findViewById(R.id.text_first_item);
            textDate = itemView.findViewById(R.id.text_second_item);

        }
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerViewFruitHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recyclerview, parent, false);
        return new RecyclerViewFruitHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerViewFruitAdapter.RecyclerViewFruitHolder holder, int position) {
        Recolte currentRecolte = listeRecolte.get(position);
        String date = beautifyStringDate(currentRecolte.getModifier_le());
        String num = String.valueOf(currentRecolte.getNum_recolte());
        String fruit_recolter;

        holder.textNumeroRecolte.setText(num);
        if(currentRecolte.getNombre_fruit_recolter() > 1){
            fruit_recolter = currentRecolte.getNombre_fruit_recolter() + " " + holder.itemView.getResources().getString(R.string.Fruits_label);
        } else {
            fruit_recolter = currentRecolte.getNombre_fruit_recolter() + " " + holder.itemView.getResources().getString(R.string.Fruit_label);
        }
        holder.textFruitRecolter.setText(fruit_recolter);

        holder.textDate.setText(date);

        // Callback simple click
        holder.itemView.setOnClickListener(v -> mOnItemListener.onItemClick(position));

        // Callback long click
        holder.itemView.setOnLongClickListener(v -> {
            mOnItemListener.onLongItemClick(position);
            return true;
        });
    }

    public void updateListe(List<Recolte> liste) {
        listeRecolte.clear();
        listeRecolte.addAll(liste);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() { return listeRecolte.size(); }
}
