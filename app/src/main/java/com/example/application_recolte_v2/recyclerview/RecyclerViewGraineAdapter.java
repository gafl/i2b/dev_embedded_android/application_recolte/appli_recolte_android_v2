package com.example.application_recolte_v2.recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.application_recolte_v2.R;
import com.example.application_recolte_v2.modele.Graine;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import static com.example.application_recolte_v2.module.Utils.beautifyStringDate;

public class RecyclerViewGraineAdapter extends RecyclerView.Adapter<RecyclerViewGraineAdapter.RecyclerViewGraineHolder> {

    private final List<Graine> listeGraine;
    private final OnItemListener mOnItemListener;

    public RecyclerViewGraineAdapter(List<Graine> listeGraine, OnItemListener mOnItemListener) {
        this.listeGraine = listeGraine;
        this.mOnItemListener = mOnItemListener;
    }

    public static class RecyclerViewGraineHolder extends RecyclerView.ViewHolder {

        private final TextView textNumeroGraine;
        private final TextView textGraineExtraite;
        private final TextView textDate;

        public RecyclerViewGraineHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            textNumeroGraine = itemView.findViewById(R.id.text_num_extraction);
            textGraineExtraite = itemView.findViewById(R.id.text_first_item_graine);
            textDate = itemView.findViewById(R.id.text_second_item_graine);
        }
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerViewGraineHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recyclerview_graine, parent, false);
        return new RecyclerViewGraineHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerViewGraineAdapter.RecyclerViewGraineHolder holder, int position) {

        Graine currentGraine = listeGraine.get(position);
        String date = beautifyStringDate(currentGraine.getModifier_le());
        String num = currentGraine.getCode_recolte().substring(0,1);
        String graine_recolter;

        holder.textNumeroGraine.setText(num);
        if(currentGraine.getNombre_graine_extraite() > 1){
            graine_recolter = currentGraine.getNombre_graine_extraite() + " " + holder.itemView.getResources().getString(R.string.extractions_label);
        } else {
            graine_recolter = currentGraine.getNombre_graine_extraite() + " " + holder.itemView.getResources().getString(R.string.extraction_label);
        }
        holder.textGraineExtraite.setText(graine_recolter);
        holder.textDate.setText(date);


        // Callback simple click
        holder.itemView.setOnClickListener(v -> mOnItemListener.onItemClick(position));

        // Callback long click
        holder.itemView.setOnLongClickListener(v -> {
            mOnItemListener.onLongItemClick(position);
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return listeGraine.size();
    }

    public void updateListe(List<Graine> liste) {
        listeGraine.clear();
        listeGraine.addAll(liste);
        notifyDataSetChanged();
    }
}
