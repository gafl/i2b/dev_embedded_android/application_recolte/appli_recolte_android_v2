package com.example.application_recolte_v2.recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.application_recolte_v2.R;
import com.example.application_recolte_v2.modele.Plante;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class RecyclerViewGeneraleAdapter extends RecyclerView.Adapter<RecyclerViewGeneraleAdapter.RecyclerViewGeneraleHolder> {

    private final List<Plante> listePlante;

    public RecyclerViewGeneraleAdapter(List<Plante> listePlante) {
        this.listePlante = listePlante;
    }

    public static class RecyclerViewGeneraleHolder extends RecyclerView.ViewHolder {

        public final TextView textCodePlante;
        public final TextView textTotalGraine;

        public RecyclerViewGeneraleHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            textCodePlante = itemView.findViewById(R.id.code_plante_item_general);
            textTotalGraine = itemView.findViewById(R.id.total_graine_item_general);
        }
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerViewGeneraleHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recyclerview_generale, parent, false);
        return new RecyclerViewGeneraleAdapter.RecyclerViewGeneraleHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerViewGeneraleAdapter.RecyclerViewGeneraleHolder holder, int position) {
        Plante currentPlante = listePlante.get(position);
        String code_plante = currentPlante.getCode_plante();
        String nombre_graine = String.valueOf(currentPlante.getNombre_graine_extraite());

        holder.textCodePlante.setText(code_plante);
        holder.textTotalGraine.setText(nombre_graine);
    }

    @Override
    public int getItemCount() { return listePlante.size(); }

    public void updateListe(List<Plante> liste) {
        listePlante.clear();
        listePlante.addAll(liste);
        notifyDataSetChanged();
    }

}
