package com.example.application_recolte_v2.request;

import android.app.Activity;
import android.content.Context;

import com.example.application_recolte_v2.R;
import com.example.application_recolte_v2.modele.ActionCallback;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.example.application_recolte_v2.modele.ActionCallback.ERROR;
import static com.example.application_recolte_v2.modele.ActionCallback.SUCCESS;
import static com.example.application_recolte_v2.modele.ActionCallback.SUCCESS_DISCONNECTION;
import static com.example.application_recolte_v2.module.SettingsHelper.deleteToken;
import static com.example.application_recolte_v2.module.SettingsHelper.getTokenStored;
import static com.example.application_recolte_v2.request.RequestUtil.WRONG_SERVER_SETTINGS;
import static com.example.application_recolte_v2.request.RequestUtil.buildUrl;
import static com.example.application_recolte_v2.request.RequestUtil.isGoodServerSetting;
import static com.example.application_recolte_v2.request.RequestUtil.isNetworkAvailable;

public class RequestUser {

    public static final String LOGIN_PATH = "/rest-auth/login/";
    public static final String LOGOUT_PATH = "/rest-auth/logout/";
    public static final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    public static void loginToServer(Context context, ActionCallback actionCallback, String username, String password){
        String url = buildUrl(context, LOGIN_PATH);

        // Verify internet connexion before
        if(!isNetworkAvailable(context)) {
            actionCallback.action(ERROR, context.getResources().getString(R.string.no_internet_detected));
            return;
        }

        // Wrong server settings
        if(!isGoodServerSetting(context)){
            actionCallback.action(WRONG_SERVER_SETTINGS, context.getResources().getString(R.string.serveur_mal_configurer));
            return;
        }

        // Create JSON data to send for connection
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("username", username);
            jsonObject.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Build client with connect timeout and read timeout
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(3, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .build();

        // Insert json data into request body
        RequestBody body = RequestBody.create(String.valueOf(jsonObject), JSON);

        // Request builder with url, method, body
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        // Request asynchrone sended
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                final String body = Objects.requireNonNull(response.body()).string();
                ((Activity) context).runOnUiThread(() -> {
                    if(response.isSuccessful()){
                        try {
                            JSONObject json = new JSONObject(body);
                            String token = json.getString("key");
                            actionCallback.action(SUCCESS, token);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        actionCallback.action(ERROR, context.getResources().getString(R.string.connexion_échoué));
                    }

                    response.close();
                });
            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                ((Activity) context).runOnUiThread(() -> {
                    if(String.valueOf(e.getMessage()).toLowerCase().contains("failed to connect")){
                        actionCallback.action(ERROR, context.getResources().getString(R.string.connexion_serveur_échoué));
                    } else {
                        actionCallback.action(ERROR, e.getMessage());
                    }
                });
            }
        });
    }

    public static void disconnectFromServeur(Context context, ActionCallback actionCallback){
        String url = buildUrl(context, LOGOUT_PATH);

        // Verify internet connexion before
        if(!isNetworkAvailable(context)) {
            actionCallback.action(ERROR, context.getResources().getString(R.string.no_internet_detected));
            return;
        }

        // Wrong server settings
        if(!isGoodServerSetting(context)){
            actionCallback.action(WRONG_SERVER_SETTINGS, context.getResources().getString(R.string.serveur_mal_configurer));
            return;
        }

        // Get back the Token
        String token = getTokenStored(context);

        // Build client with connect timeout and read timeout
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(3, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .build();


        // Create empty body for post request
        RequestBody body = RequestBody.create(new byte[]{}, null);

        // Request builder with url, method, body
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Authorization", "Token " + token)
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) {
                ((Activity) context).runOnUiThread(() -> {
                    deleteToken(context);
                    actionCallback.action(SUCCESS_DISCONNECTION, context.getResources().getString(R.string.deconnexion_succes));

                    response.close();
                });
            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                ((Activity) context).runOnUiThread(() -> {
                    if(String.valueOf(e.getMessage()).toLowerCase().contains("failed to connect")){
                        actionCallback.action(ERROR, context.getResources().getString(R.string.connexion_serveur_échoué));
                    } else {
                        actionCallback.action(ERROR, e.getMessage());
                    }
                });
            }
        });
    }
}
