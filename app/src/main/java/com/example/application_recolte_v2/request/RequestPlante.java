package com.example.application_recolte_v2.request;

import android.app.Activity;
import android.content.Context;

import com.example.application_recolte_v2.R;
import com.example.application_recolte_v2.modele.ActionCallback;
import com.example.application_recolte_v2.modele.Plante;
import com.example.application_recolte_v2.sqlite.DatabaseHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.example.application_recolte_v2.modele.ActionCallback.ERROR;
import static com.example.application_recolte_v2.modele.ActionCallback.GET_RECOLTE;
import static com.example.application_recolte_v2.modele.ActionCallback.SEND_PLANTE;
import static com.example.application_recolte_v2.module.SettingsHelper.deleteToken;
import static com.example.application_recolte_v2.module.SettingsHelper.getTokenStored;
import static com.example.application_recolte_v2.module.SettingsHelper.isConnected;
import static com.example.application_recolte_v2.request.RequestUser.JSON;
import static com.example.application_recolte_v2.request.RequestUtil.NOT_CONNECTED;
import static com.example.application_recolte_v2.request.RequestUtil.buildUrl;
import static com.example.application_recolte_v2.request.RequestUtil.isNetworkAvailable;
import static com.example.application_recolte_v2.request.RequestUtil.testIfAllIsGoodBeforeRequest;
import static com.example.application_recolte_v2.sqlite.PlanteTableHelper.convertListPlanteToJsonArray;

public class RequestPlante {

    public static final String GET_PLANTE_PATH = "/plante/";
    public static final String ADD_PLANTE_PATH = "/plante_add/";

    public static void getPlanteFromServerToLocale(Context context, ActionCallback actionCallback){
        String url = buildUrl(context, GET_PLANTE_PATH);

        // Test internet connexion then server settings and finally if we are logged in server
        if (!testIfAllIsGoodBeforeRequest(context, actionCallback)) { return; }

        // Get back the Token
        String token = getTokenStored(context);

        // Build client with connect timeout and read timeout
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(3, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .build();

        // Request builder with Authorization, url, method
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Authorization", "Token " + token)
                .get()
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                final String myResponse = Objects.requireNonNull(response.body()).string();

                ((Activity) context).runOnUiThread(() -> {
                    if(response.isSuccessful()){
                        List<Plante> listePlante;

                        Gson gson = new Gson();
                        TypeToken<List<Plante>> token = new TypeToken<List<Plante>>() {};
                        listePlante = gson.fromJson(myResponse, token.getType());

                        DatabaseHelper databaseHelper = new DatabaseHelper(context);

                        for(Plante plante : listePlante){
                            if(!databaseHelper.rowPlanteExistByCodePlante(plante.getCode_plante())){
                                databaseHelper.insertOnPlante(plante);
                            } else {
                                databaseHelper.updateOnPlante(plante);
                            }
                        }

                        actionCallback.action(SEND_PLANTE, "succes");

                    } else {
                        if(myResponse.toLowerCase().contains("invalid token")){
                            deleteToken(context);
                            actionCallback.action(NOT_CONNECTED, context.getResources().getString(R.string.deconnecter_sur_autre_appareil));
                        } else {
                            actionCallback.action(ERROR, context.getResources().getString(R.string.recuperation_table_plante_echoue));
                        }
                    }
                    response.close();
                });
            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                ((Activity) context).runOnUiThread(() -> {
                    if(String.valueOf(e.getMessage()).toLowerCase().contains("failed to connect")){
                        actionCallback.action(ERROR, context.getResources().getString(R.string.connexion_serveur_échoué));
                    } else {
                        actionCallback.action(ERROR, e.getMessage());
                    }
                });
            }
        });
    }

    public static void sendPlanteFromLocaleToServer(Context context, ActionCallback actionCallback){
        String url = buildUrl(context, ADD_PLANTE_PATH);

        // Test internet connexion then server settings and finally if we are logged in server
        if (!testIfAllIsGoodBeforeRequest(context, actionCallback)) { return; }

        // Get back the Token
        String token = getTokenStored(context);

        // Build client with connect timeout and read timeout
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(3, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .build();

        // Get all plante from locale and convert it to JsonObject
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        JSONObject jsonObject = convertListPlanteToJsonArray(databaseHelper.getAllPlante());

        // Insert json data into request body
        RequestBody body = RequestBody.create(String.valueOf(jsonObject), JSON);

        // Request builder with url, method, body
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Authorization", "Token " + token)
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                String myResponse = Objects.requireNonNull(response.body()).string();
                ((Activity) context).runOnUiThread(() -> {
                    if(response.isSuccessful()){
                        actionCallback.action(GET_RECOLTE, "succès");
                    } else {
                        if(myResponse.toLowerCase().contains("invalid token")){
                            deleteToken(context);
                            actionCallback.action(NOT_CONNECTED, context.getResources().getString(R.string.deconnecter_sur_autre_appareil));
                        } else {
                            actionCallback.action(ERROR, context.getResources().getString(R.string.recuperation_table_recolte_echoue));
                        }
                    }
                    response.close();
                });
            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                ((Activity) context).runOnUiThread(() -> {
                    if(String.valueOf(e.getMessage()).toLowerCase().contains("failed to connect")){
                        actionCallback.action(ERROR, context.getResources().getString(R.string.connexion_serveur_échoué));
                    } else {
                        actionCallback.action(ERROR, e.getMessage());
                    }
                });
            }
        });
    }
}
