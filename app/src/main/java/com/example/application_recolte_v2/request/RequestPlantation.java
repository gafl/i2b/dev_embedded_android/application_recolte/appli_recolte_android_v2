package com.example.application_recolte_v2.request;

import android.app.Activity;
import android.content.Context;

import com.example.application_recolte_v2.R;
import com.example.application_recolte_v2.modele.ActionCallback;
import com.example.application_recolte_v2.modele.Plantation;
import com.example.application_recolte_v2.sqlite.DatabaseHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.example.application_recolte_v2.modele.ActionCallback.ERROR;
import static com.example.application_recolte_v2.modele.ActionCallback.GET_PLANTATION;
import static com.example.application_recolte_v2.modele.ActionCallback.GET_PLANTE;
import static com.example.application_recolte_v2.module.SettingsHelper.deleteToken;
import static com.example.application_recolte_v2.module.SettingsHelper.getTokenStored;
import static com.example.application_recolte_v2.module.SettingsHelper.isConnected;
import static com.example.application_recolte_v2.request.RequestUser.JSON;
import static com.example.application_recolte_v2.request.RequestUtil.NOT_CONNECTED;
import static com.example.application_recolte_v2.request.RequestUtil.buildUrl;
import static com.example.application_recolte_v2.request.RequestUtil.isNetworkAvailable;
import static com.example.application_recolte_v2.request.RequestUtil.testIfAllIsGoodBeforeRequest;
import static com.example.application_recolte_v2.sqlite.PlantationTableHelper.convertListPlantationToJsonObject;

public class RequestPlantation {

    public static final String GET_PLANTATION_PATH = "/plantation/";
    public static final String ADD_PLANTATION_PATH = "/plantation_add/";

    public static void getPlantationFromServeurToLocale(Context context, ActionCallback actionCallback){
        String url = buildUrl(context, GET_PLANTATION_PATH);

        // Test internet connexion then server settings and finally if we are logged in server
        if (!testIfAllIsGoodBeforeRequest(context, actionCallback)) { return; }

        // Get back the Token
        String token = getTokenStored(context);

        // Build client with connect timeout and read timeout
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(3, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .build();

        // Request builder with Authorization, url, method
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Authorization", "Token " + token)
                .get()
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                final String myResponse = Objects.requireNonNull(response.body()).string();

                ((Activity) context).runOnUiThread(() -> {
                    if(response.isSuccessful()){
                        List<Plantation> listePlantation;

                        Gson gson = new Gson();
                        TypeToken<List<Plantation>> token = new TypeToken<List<Plantation>>() {};
                        listePlantation = gson.fromJson(myResponse, token.getType());

                        DatabaseHelper databaseHelper = new DatabaseHelper(context);

                        for(Plantation plantation : listePlantation){
                            if(!databaseHelper.rowPlantationExistByCodeLotPlante(plantation.getCode_lot_plante())){
                                databaseHelper.insertOnPlantation(plantation);
                            } else {
                                databaseHelper.updateOnPlantation(plantation);
                            }
                        }

                        actionCallback.action(GET_PLANTE, context.getResources().getString(R.string.synchronisation_succès));

                    } else {
                        if(myResponse.toLowerCase().contains("invalid token")){
                            deleteToken(context);
                            actionCallback.action(NOT_CONNECTED, context.getResources().getString(R.string.deconnecter_sur_autre_appareil));
                        } else {
                            actionCallback.action(ERROR, context.getResources().getString(R.string.recuperation_table_plantation_echoue));
                        }
                    }
                    response.close();
                });
            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                ((Activity) context).runOnUiThread(() -> {
                    if(String.valueOf(e.getMessage()).toLowerCase().contains("failed to connect")){
                        actionCallback.action(ERROR, context.getResources().getString(R.string.connexion_serveur_échoué));
                    } else {
                        actionCallback.action(ERROR, e.getMessage());
                    }
                });
            }
        });
    }

    public static void sendPlantationsToServer(Context context, ActionCallback actionCallback, List<Plantation> listePlantation){
        String url = buildUrl(context, ADD_PLANTATION_PATH);

        // Test internet connexion then server settings and finally if we are logged in server
        if (!testIfAllIsGoodBeforeRequest(context, actionCallback)) { return; }

        // Get back the Token
        String token = getTokenStored(context);

        // Build client with connect timeout and read timeout
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(3, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .build();

        // Convert Liste to JsonObject
        JSONObject jsonObject = convertListPlantationToJsonObject(listePlantation);

        // Insert json data into request body
        RequestBody body = RequestBody.create(String.valueOf(jsonObject), JSON);

        // Request builder with url, method, body
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Authorization", "Token " + token)
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                String myResponse = Objects.requireNonNull(response.body()).string();
                ((Activity) context).runOnUiThread(() -> {
                    if(response.isSuccessful()){
                        actionCallback.action(GET_PLANTATION, "succès");
                    } else {
                        if(myResponse.toLowerCase().contains("invalid token")){
                            deleteToken(context);
                            actionCallback.action(NOT_CONNECTED, context.getResources().getString(R.string.deconnecter_sur_autre_appareil));
                        } else {
                            actionCallback.action(ERROR, context.getResources().getString(R.string.envoie_fichier_xls));
                        }
                    }
                    response.close();
                });
            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                ((Activity) context).runOnUiThread(() -> {
                    if(String.valueOf(e.getMessage()).toLowerCase().contains("failed to connect")){
                        actionCallback.action(ERROR, context.getResources().getString(R.string.connexion_serveur_échoué));
                    } else {
                        actionCallback.action(ERROR, e.getMessage());
                    }
                });
            }
        });

    }
}
