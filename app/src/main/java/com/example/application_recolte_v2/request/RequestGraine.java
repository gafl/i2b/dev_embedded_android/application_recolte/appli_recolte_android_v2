package com.example.application_recolte_v2.request;

import android.app.Activity;
import android.content.Context;

import com.example.application_recolte_v2.R;
import com.example.application_recolte_v2.modele.ActionCallback;
import com.example.application_recolte_v2.modele.Graine;
import com.example.application_recolte_v2.sqlite.DatabaseHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.example.application_recolte_v2.modele.ActionCallback.ERROR;
import static com.example.application_recolte_v2.modele.ActionCallback.SEND_GRAINE;
import static com.example.application_recolte_v2.modele.ActionCallback.SUCCESS;
import static com.example.application_recolte_v2.module.SettingsHelper.deleteToken;
import static com.example.application_recolte_v2.module.SettingsHelper.getTokenStored;
import static com.example.application_recolte_v2.request.RequestUser.JSON;
import static com.example.application_recolte_v2.request.RequestUtil.NOT_CONNECTED;
import static com.example.application_recolte_v2.request.RequestUtil.buildUrl;
import static com.example.application_recolte_v2.request.RequestUtil.testIfAllIsGoodBeforeRequest;
import static com.example.application_recolte_v2.sqlite.GraineTableHelper.convertListGraineToJsonArray;

public class RequestGraine {

    public static final String GET_GRAINE_PATH = "/graine/";
    public static final String ADD_GRAINE_PATH = "/graine_add/";

    public static void getGraineFromServerToLocale(Context context, ActionCallback actionCallback){
        String url = buildUrl(context, GET_GRAINE_PATH);

        // Test internet connexion then server settings and finally if we are logged in server
        if (!testIfAllIsGoodBeforeRequest(context, actionCallback)) { return; }

        // Get back the Token
        String token = getTokenStored(context);

        // Build client with connect timeout and read timeout
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(3, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .build();

        // Request builder with Authorization, url, method
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Authorization", "Token " + token)
                .get()
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                final String myResponse = Objects.requireNonNull(response.body()).string();

                ((Activity) context).runOnUiThread(() -> {
                    if(response.isSuccessful()){
                        List<Graine> listeGraine;

                        Gson gson = new Gson();
                        TypeToken<List<Graine>> token = new TypeToken<List<Graine>>() {};
                        listeGraine = gson.fromJson(myResponse, token.getType());

                        DatabaseHelper databaseHelper = new DatabaseHelper(context);

                        for(Graine graine : listeGraine){
                            if(!databaseHelper.rowGraineExistByCodeGraine(graine.getCode_graine())){
                                databaseHelper.insertOnGraine(graine);
                            } else {
                                databaseHelper.updateOnGraine(graine);
                            }
                        }

                        actionCallback.action(SEND_GRAINE, "succes");

                    } else {
                        if(myResponse.toLowerCase().contains("invalid token")){
                            deleteToken(context);
                            actionCallback.action(NOT_CONNECTED, context.getResources().getString(R.string.deconnecter_sur_autre_appareil));
                        } else {
                            actionCallback.action(ERROR, context.getResources().getString(R.string.recuperation_table_plante_echoue));
                        }
                    }
                    response.close();
                });
            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                ((Activity) context).runOnUiThread(() -> {
                    if(String.valueOf(e.getMessage()).toLowerCase().contains("failed to connect")){
                        actionCallback.action(ERROR, context.getResources().getString(R.string.connexion_serveur_échoué));
                    } else {
                        actionCallback.action(ERROR, e.getMessage());
                    }
                });
            }
        });
    }

    public static void sendGraineFromLocaleToServer(Context context, ActionCallback actionCallback){
        String url = buildUrl(context, ADD_GRAINE_PATH);

        // Test internet connexion then server settings and finally if we are logged in server
        if (!testIfAllIsGoodBeforeRequest(context, actionCallback)) { return; }

        // Get back the Token
        String token = getTokenStored(context);

        // Build client with connect timeout and read timeout
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(3, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .build();

        // Get all plante from locale and convert it to JsonObject
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        JSONObject jsonObject = convertListGraineToJsonArray(databaseHelper.getAllGraine());

        // Insert json data into request body
        RequestBody body = RequestBody.create(String.valueOf(jsonObject), JSON);

        // Request builder with url, method, body
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Authorization", "Token " + token)
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                String myResponse = Objects.requireNonNull(response.body()).string();
                ((Activity) context).runOnUiThread(() -> {
                    if(response.isSuccessful()){
                        actionCallback.action(SUCCESS, context.getResources().getString(R.string.synchronisation_succès));
                    } else {
                        if(myResponse.toLowerCase().contains("invalid token")){
                            deleteToken(context);
                            actionCallback.action(NOT_CONNECTED, context.getResources().getString(R.string.deconnecter_sur_autre_appareil));
                        } else {
                            actionCallback.action(ERROR, context.getResources().getString(R.string.recuperation_table_graine_echoue));
                        }
                    }
                    response.close();
                });
            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                ((Activity) context).runOnUiThread(() -> {
                    if(String.valueOf(e.getMessage()).toLowerCase().contains("failed to connect")){
                        actionCallback.action(ERROR, context.getResources().getString(R.string.connexion_serveur_échoué));
                    } else {
                        actionCallback.action(ERROR, e.getMessage());
                    }
                });
            }
        });
    }
}
