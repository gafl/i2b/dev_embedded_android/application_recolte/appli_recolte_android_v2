package com.example.application_recolte_v2.request;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.util.Log;

import androidx.preference.PreferenceManager;

import com.example.application_recolte_v2.R;
import com.example.application_recolte_v2.modele.ActionCallback;
import com.example.application_recolte_v2.module.SettingsHelper;

import java.io.IOException;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.example.application_recolte_v2.modele.ActionCallback.ERROR;
import static com.example.application_recolte_v2.module.SettingsHelper.isConnected;

public class RequestUtil {
    public static final int NOT_CONNECTED = 5;
    public static final int WRONG_SERVER_SETTINGS = 174;

    public static String buildUrl(Context context, String path){
        String ip = SettingsHelper.getIpFromPreference(context);
        String port = SettingsHelper.getPortFromPreference(context);
        return "http://" + ip + ":" + port + path;
    }

    public static Boolean isNetworkAvailable(Context context) {
            try {
                HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                urlc.setRequestProperty("User-Agent", "Test");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1500);
                urlc.connect();
                return (urlc.getResponseCode() == 200);
            } catch (IOException e) {
                return false;
            }
    }

    public static boolean isGoodServerSetting(Context context){
        String ip = SettingsHelper.getIpFromPreference(context);
        String port = SettingsHelper.getPortFromPreference(context);

        return !ip.equals("none") && !port.equals("none");
    }

    public static boolean testIfAllIsGoodBeforeRequest(Context context, ActionCallback actionCallback){

        // Verify internet connexion before
        if(!isNetworkAvailable(context)) {
            actionCallback.action(ERROR, context.getResources().getString(R.string.no_internet_detected));
            return false;
        }

        // Wrong server settings
        if(!isGoodServerSetting(context)){
            actionCallback.action(WRONG_SERVER_SETTINGS, context.getResources().getString(R.string.serveur_mal_configurer));
            return false;
        }

        // Verify if the user is connected
        if(!isConnected(context)){
            actionCallback.action(NOT_CONNECTED, context.getResources().getString(R.string.not_connected));
            return false;
        }

        return true;
    }

    public static boolean testIfAllIsGoodBeforeConnection(Context context, ActionCallback actionCallback){

        // Verify internet connexion before
        if(!isNetworkAvailable(context)) {
            actionCallback.action(ERROR, context.getResources().getString(R.string.no_internet_detected));
            return false;
        }

        // Wrong server settings
        if(!isGoodServerSetting(context)){
            actionCallback.action(WRONG_SERVER_SETTINGS, context.getResources().getString(R.string.serveur_mal_configurer));
            return false;
        }

        return true;
    }
}
