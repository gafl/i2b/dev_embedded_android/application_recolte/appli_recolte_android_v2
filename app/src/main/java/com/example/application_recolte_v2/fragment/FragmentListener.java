package com.example.application_recolte_v2.fragment;

import com.example.application_recolte_v2.modele.Plantation;
import com.example.application_recolte_v2.modele.Plante;

public interface FragmentListener {
    Plantation get_plantation_from_activity_container();
    Plante get_plante_from_activity_container();
}
