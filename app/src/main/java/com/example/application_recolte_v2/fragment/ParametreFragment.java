package com.example.application_recolte_v2.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.preference.DropDownPreference;
import androidx.preference.EditTextPreference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SwitchPreference;

import com.example.application_recolte_v2.activity.ParametreActivity;
import com.example.application_recolte_v2.R;

import static com.example.application_recolte_v2.module.SweetAlert.errorAlert;
import static com.example.application_recolte_v2.module.Utils.restartActivity;
import static com.example.application_recolte_v2.module.Utils.setAppLanguage;

public class ParametreFragment extends PreferenceFragmentCompat {

    private ParametreActivity myContext;
    private OnDataPass dataPasser;

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.parametres, rootKey);

        passData(1500);

        SwitchPreference switchPreference = findPreference("mode_sombre_pref");
        DropDownPreference dropDownPreference = findPreference("langage_pref");
        EditTextPreference edit_ip = findPreference("ip_parametre");
        EditTextPreference edit_port = findPreference("port_parametre");

        assert edit_ip != null;
        edit_ip.setOnPreferenceChangeListener((preference, newValue) -> {
            if(Patterns.IP_ADDRESS.matcher((CharSequence) newValue).matches()){
                return true;
            }else if(Patterns.WEB_URL.matcher((CharSequence) newValue).matches()){
                return true;
            } else{
                errorAlert(getString(R.string.ip_serveur_non_valide), myContext);
                return false;
            }
        });

        assert edit_port != null;
        edit_port.setOnPreferenceChangeListener((preference, newValue) -> {
            String val = (String) newValue;
            if(Integer.parseInt(val) > 0 && Integer.parseInt(val) < 65536){
                return true;
            } else {
                errorAlert(getString(R.string.port_serveur_non_valide), myContext);
                return false;
            }
        });

        edit_port.setOnBindEditTextListener(editText -> editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();

                if(!text.matches("^[0-9]*$")){
                    text = text.replaceAll("[^\\d]", "");
                    editText.setText(text);
                    editText.setSelection(text.length());
                }

                if(text.length() > 5){
                    text = text.substring(0,5);
                    editText.setText(text);
                    editText.setSelection(text.length());
                }
            }
        }));

        assert switchPreference != null;
        switchPreference.setOnPreferenceChangeListener((preference, isOn) -> {
            if((boolean) isOn){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }
            return true;
        });

        assert dropDownPreference != null;
        dropDownPreference.setOnPreferenceChangeListener(((preference, newValue) -> {
            setAppLanguage(requireActivity(), (String) newValue);
            restartActivity(requireActivity());
            final Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(() -> passData(Activity.RESULT_OK), 200);
            return true;
        }));
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        myContext= (ParametreActivity) context;
        dataPasser = (OnDataPass) context;
    }

    public void passData(int data) {
        dataPasser.onDataPass(data);
    }

    public interface OnDataPass {
        void onDataPass(int code);
    }
}
