package com.example.application_recolte_v2.fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.application_recolte_v2.R;
import com.example.application_recolte_v2.modele.Graine;
import com.example.application_recolte_v2.modele.Recolte;
import com.example.application_recolte_v2.module.SettingsHelper;
import com.example.application_recolte_v2.sqlite.DatabaseHelper;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.Objects;

public class ItemRecolteDialogFragment extends DialogFragment {

    private boolean isAdding;
    private String code_plante, num_recolte, nombre_fruit, code_recolte;
    private EditText editTextNombreFruits;
    private Context myContext;
    private Button buttonValider;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        isAdding = getArguments().getBoolean("isAdding");
        code_plante = getArguments().getString("code_plante");
        num_recolte = getArguments().getString("num_recolte");
        nombre_fruit = getArguments().getString("nombre_fruit");
        code_recolte = getArguments().getString("code_recolte");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_modifier_item, container, false);

        TextView textViewNumRecolte = rootView.findViewById(R.id.textView_Num_Recolte_Dialog);
        editTextNombreFruits = rootView.findViewById(R.id.editText_nombre_fruit_recolter);
        buttonValider = rootView.findViewById(R.id.button_valider_modifier_item_recolte);
        Button buttonAnnuler = rootView.findViewById(R.id.button_annuler_modifier_item_recolte);

        enableButton(buttonValider, false);

        DatabaseHelper databaseHelper = new DatabaseHelper(myContext);

        if(isAdding){
            num_recolte = String.valueOf(databaseHelper.countRowRecolteByCodePlante(code_plante) + 1);
            textViewNumRecolte.setText(num_recolte);
        } else{
            textViewNumRecolte.setText(num_recolte);
            editTextNombreFruits.setText(nombre_fruit);
        }

        buttonAnnuler.setOnClickListener(view -> Objects.requireNonNull(getDialog()).dismiss());

        editTextNombreFruits.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if(isAdding) {
                    enableButton(buttonValider, s.toString().length() > 0);
                } else enableButton(buttonValider, s.toString().length() > 0 && !s.toString().equals(nombre_fruit));
            }
        });

        buttonValider.setOnClickListener(v -> {
            int nbFruits = Integer.parseInt(editTextNombreFruits.getText().toString());
            OffsetDateTime currentDate = OffsetDateTime.now(ZoneId.of("Europe/Paris"));
            Recolte recolte;

            if(isAdding) {
                code_recolte = num_recolte + code_plante;
                String code_graine = num_recolte + num_recolte + code_plante;
                recolte = new Recolte(code_plante, code_recolte, Integer.parseInt(num_recolte), nbFruits, currentDate.toString());
                Graine graine = new Graine(code_recolte, code_graine, 0, currentDate.toString());
                databaseHelper.insertOnRecolte(recolte);
                databaseHelper.insertOnGraine(graine);
            }
            else{
                recolte = new Recolte(code_plante, code_recolte, Integer.parseInt(num_recolte), nbFruits, currentDate.toString());
                databaseHelper.updateOnRecolte(recolte);
            }

            SettingsHelper.databaseHasChanged(myContext);

            Objects.requireNonNull(getDialog()).dismiss();
        });

        return rootView;
    }

    private void enableButton(Button button, boolean bool){
        button.setEnabled(bool);
        if(!bool)
            button.setBackgroundColor(button.getContext().getResources().getColor(R.color.disable, myContext.getTheme()));
        else
            button.setBackgroundColor(button.getContext().getResources().getColor(R.color.valider_color, myContext.getTheme()));
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDimensionPixelSize(R.dimen.popup_width);
        int height = getResources().getDimensionPixelSize(R.dimen.popup_height);
        Objects.requireNonNull(getDialog()).getWindow().setLayout(width, height);
        getDialog().setCancelable(false);
        getDialog().setCanceledOnTouchOutside(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        dismissAllowingStateLoss();
    }

    @Override
    public void onStop() {
        super.onStop();
        dismissAllowingStateLoss();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        myContext = context;
    }
}
