package com.example.application_recolte_v2.fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.application_recolte_v2.R;
import com.example.application_recolte_v2.modele.Graine;
import com.example.application_recolte_v2.module.SettingsHelper;
import com.example.application_recolte_v2.sqlite.DatabaseHelper;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.Objects;

public class ItemGrainDialogFragment extends DialogFragment {

    private Context myContext;
    private String code_recolte, code_graine, nombre_graine, num_recolte;
    private EditText editNombreGraine;
    private Button buttonValider;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        num_recolte = getArguments().getString("num_recolte");
        code_recolte = getArguments().getString("code_recolte");
        code_graine = getArguments().getString("code_graine");
        nombre_graine = getArguments().getString("nombre_graine");
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_modifier_item_graine, container, false);

        editNombreGraine = rootView.findViewById(R.id.editText_nombre_graine_extraite);
        buttonValider = rootView.findViewById(R.id.button_valider_modifier_item_graine);
        Button buttonAnnuler = rootView.findViewById(R.id.button_annuler_modifier_graine);
        buttonAnnuler.setOnClickListener(view -> Objects.requireNonNull(getDialog()).dismiss());
        TextView textViewNumRecolte = rootView.findViewById(R.id.textView_num_recolte_graine);
        textViewNumRecolte.setText(num_recolte);

        enableButton(buttonValider, false);

        DatabaseHelper databaseHelper = new DatabaseHelper(myContext);

        editNombreGraine.setText(nombre_graine);

        editNombreGraine.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                enableButton(buttonValider, s.toString().length() > 0 && !s.toString().equals(nombre_graine));
            }
        });

        buttonValider.setOnClickListener(v -> {
            int nbGraine = Integer.parseInt(editNombreGraine.getText().toString());
            OffsetDateTime currentDate = OffsetDateTime.now(ZoneId.of("Europe/Paris"));
            SettingsHelper.databaseHasChanged(myContext);
            databaseHelper.updateOnGraine(new Graine(code_recolte, code_graine, nbGraine, currentDate.toString()));

            Objects.requireNonNull(getDialog()).dismiss();
        });

        return rootView;
    }

    private void enableButton(Button button, boolean bool){
        button.setEnabled(bool);
        if(!bool)
            button.setBackgroundColor(button.getContext().getResources().getColor(R.color.disable, myContext.getTheme()));
        else
            button.setBackgroundColor(button.getContext().getResources().getColor(R.color.valider_color, myContext.getTheme()));
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDimensionPixelSize(R.dimen.popup_width);
        int height = getResources().getDimensionPixelSize(R.dimen.popup_height);
        Objects.requireNonNull(getDialog()).getWindow().setLayout(width, height);
        getDialog().setCancelable(false);
        getDialog().setCanceledOnTouchOutside(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        dismissAllowingStateLoss();
    }

    @Override
    public void onStop() {
        super.onStop();
        dismissAllowingStateLoss();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        myContext = context;
    }
}
