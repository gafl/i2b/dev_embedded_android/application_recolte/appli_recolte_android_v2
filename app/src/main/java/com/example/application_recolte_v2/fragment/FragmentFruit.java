package com.example.application_recolte_v2.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.application_recolte_v2.activity.BluetoothScannerActivity;
import com.example.application_recolte_v2.activity.FragmentContainer;
import com.example.application_recolte_v2.R;
import com.example.application_recolte_v2.modele.ActionCallback;
import com.example.application_recolte_v2.modele.Plantation;
import com.example.application_recolte_v2.modele.Plante;
import com.example.application_recolte_v2.modele.Recolte;
import com.example.application_recolte_v2.module.SettingsHelper;
import com.example.application_recolte_v2.module.SweetAlert;
import com.example.application_recolte_v2.module.ZebraPrint;
import com.example.application_recolte_v2.recyclerview.OnItemListener;
import com.example.application_recolte_v2.recyclerview.RecyclerViewFruitAdapter;
import com.example.application_recolte_v2.sqlite.DatabaseHelper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.example.application_recolte_v2.module.PermissionChecker.requestBluetoothPermission;
import static com.example.application_recolte_v2.module.SweetAlert.errorAlert;
import static com.example.application_recolte_v2.module.SweetAlert.successAlert;


public class FragmentFruit extends Fragment implements OnItemListener, SweetAlert.CallbackSweetAlert, ActionCallback {

    private FragmentListener mCallback;
    private Plante plante;
    private FragmentContainer myContext;
    private List<Recolte> listeRecolte;
    private DatabaseHelper databaseHelper;
    private RecyclerViewFruitAdapter adapter;
    private Button buttonPrint;
    private TextView textView_aucune_recolte;
    private ImageView image_aucun_recolte;
    private ItemRecolteDialogFragment itemRecolteDialogFragment;
    private String code_recolte_to_delete;
    private ActivityResultLauncher<Intent> activityResultBluetooth;
    private SweetAlertDialog pDialog;

    public FragmentFruit() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_fruit, container, false);

        databaseHelper = new DatabaseHelper(rootView.getContext());

        plante = mCallback.get_plante_from_activity_container();
        listeRecolte = new ArrayList<>();
        listeRecolte = databaseHelper.getAllRecolteByCodePlante(plante.getCode_plante());

        FloatingActionButton addButton = rootView.findViewById(R.id.floating_add_button);
        RecyclerView recyclerView = rootView.findViewById(R.id.recyclerView_fruits);
        buttonPrint = rootView.findViewById(R.id.button_imprimer_etiquette_recolte);
        textView_aucune_recolte = rootView.findViewById(R.id.textView_vide_recolte);
        image_aucun_recolte = rootView.findViewById(R.id.fruit_no_recolte);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(rootView.getContext());
        adapter = new RecyclerViewFruitAdapter(listeRecolte, this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        ChangeVisibilityElementIfListRecolteEmpty();

        addButton.setOnClickListener(view -> {
            itemRecolteDialogFragment = newInstance(plante.getCode_plante(), true, null);
            itemRecolteDialogFragment.show(myContext.getSupportFragmentManager(), "Modifer fragment");

            myContext.getSupportFragmentManager().executePendingTransactions();
            Objects.requireNonNull(itemRecolteDialogFragment.getDialog()).setOnDismissListener(dialog -> updateRecyclerView());
        });

        buttonPrint.setOnClickListener(view -> requestBluetoothPermission(myContext, this));

        // Activity result from bluetooth activity
        activityResultBluetooth = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        assert result.getData() != null;
                        String macAddress = result.getData().getStringExtra("mac");
                        SettingsHelper.saveBluetoothAddress(myContext, macAddress);
                        launchImpression(macAddress);
                    }
                });

        return rootView;
    }

    private void ChangeVisibilityElementIfListRecolteEmpty() {
        if(listeRecolte.isEmpty()){
            buttonPrint.setVisibility(View.INVISIBLE);
            textView_aucune_recolte.setVisibility(View.VISIBLE);
            image_aucun_recolte.setVisibility(View.VISIBLE);
        } else{
            buttonPrint.setVisibility(View.VISIBLE);
            textView_aucune_recolte.setVisibility(View.INVISIBLE);
            image_aucun_recolte.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        mCallback = (FragmentListener) context;
        myContext = (FragmentContainer) context;
    }

    @SuppressWarnings("unused")
    public static FragmentFruit newInstance() {
        return new FragmentFruit();
    }

    @Override
    public void onItemClick(int position) {
        Recolte recolte = listeRecolte.get(position);
        itemRecolteDialogFragment = newInstance(recolte.getCode_plante(), false, recolte);
        itemRecolteDialogFragment.show(myContext.getSupportFragmentManager(), "Modifier fragment");

        myContext.getSupportFragmentManager().executePendingTransactions();
        Objects.requireNonNull(itemRecolteDialogFragment.getDialog()).setOnDismissListener(dialog -> updateRecyclerView());
    }

    @Override
    public void onLongItemClick(int position) {
        Recolte recolte = listeRecolte.get(position);
        if(position == listeRecolte.size()-1){
            code_recolte_to_delete = recolte.getCode_recolte();
            SweetAlert.choiceAlert(myContext.getResources().getString(R.string.etes_vous_sur),myContext.getResources().getString(R.string.delete_item_fruit) + recolte.getNum_recolte(), myContext, this, 0);
        } else {
            SweetAlert.errorAlert(myContext.getResources().getString(R.string.derniere_recolte_supprimer), myContext);
        }
    }

    public void updateRecyclerView(){
        if(adapter != null && listeRecolte != null){
            adapter.updateListe(databaseHelper.getAllRecolteByCodePlante(plante.getCode_plante()));
            ChangeVisibilityElementIfListRecolteEmpty();
        }
    }

    public static ItemRecolteDialogFragment newInstance(String code_plante, boolean isAdding, Recolte recolte){
        ItemRecolteDialogFragment itemRecolteDialogFragment = new ItemRecolteDialogFragment();

        String num_recolte, nombre_fruit, code_recolte;

        if(recolte == null){
            num_recolte = "NONE";
            nombre_fruit = "NONE";
            code_recolte = "NONE";
        } else{
            num_recolte = String.valueOf(recolte.getNum_recolte());
            nombre_fruit = String.valueOf(recolte.getNombre_fruit_recolter());
            code_recolte = recolte.getCode_recolte();
        }

        Bundle args = new Bundle();
        args.putBoolean("isAdding", isAdding);
        args.putString("code_plante", code_plante);
        args.putString("num_recolte", num_recolte);
        args.putString("nombre_fruit", nombre_fruit);
        args.putString("code_recolte", code_recolte);
        itemRecolteDialogFragment.setArguments(args);

        return itemRecolteDialogFragment;
    }

    @Override
    public void onActionSweetAlert(boolean bool, int request) {
        if(bool){
            databaseHelper.deleteRecolteAndGraineByCodeRecolte(code_recolte_to_delete);
            updateRecyclerView();
        }
    }

    @Override
    public void action(int code, String message) {
        if(ActionCallback.PERMISSION_BLUETOOTH_GRANTED == code){
            if(SettingsHelper.getBluetoothAddress(myContext).equals("none")) {
                activityResultBluetooth.launch(new Intent(myContext, BluetoothScannerActivity.class));
            } else {
                launchImpression(SettingsHelper.getBluetoothAddress(myContext));
            }
        } else if (ActionCallback.SUCCESS == code){
            pDialog.dismiss();
            successAlert(message, myContext);
        } else if (ActionCallback.ERROR == code){
            pDialog.dismiss();
            errorAlert(message, myContext);
        }
    }

    public void launchImpression(String macAddress){
        pDialog = new SweetAlertDialog(myContext, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(myContext.getResources().getString(R.string.impression_en_cours));
        pDialog.setCancelable(false);
        pDialog.show();
        ZebraPrint zebraPrint = new ZebraPrint(this, myContext);
        Plantation plantation = databaseHelper.getPlantationByCodeLotPlante(plante.getCode_lot_plante());
        Recolte recolte = databaseHelper.getLastRecolteFromPlanteByCodePlante(plante.getCode_plante());
        zebraPrint.print(macAddress, plante, plantation, recolte, false);
    }
}