package com.example.application_recolte_v2.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.application_recolte_v2.R;
import com.example.application_recolte_v2.modele.Plantation;
import com.example.application_recolte_v2.modele.Plante;
import com.example.application_recolte_v2.recyclerview.RecyclerViewGeneraleAdapter;
import com.example.application_recolte_v2.sqlite.DatabaseHelper;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;


public class FragmentGenerale extends Fragment {

    private FragmentListener mCallback;
    private Plante plante;
    private Plantation plantation;
    private List<Plante> listePlante;
    private DatabaseHelper databaseHelper;
    private RecyclerViewGeneraleAdapter adapter;
    TextView text_fruit, text_graine, text_total_graine;

    public FragmentGenerale(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_generale, container, false);

        plante = mCallback.get_plante_from_activity_container();
        plantation = mCallback.get_plantation_from_activity_container();
        databaseHelper = new DatabaseHelper(rootView.getContext());

        TextView text_accession = rootView.findViewById(R.id.textView_accession);
        TextView text_parcelle = rootView.findViewById(R.id.textView_parcelle);
        TextView text_num_plante = rootView.findViewById(R.id.textView_num_plante);
        TextView text_type = rootView.findViewById(R.id.textView_type_fecondation);
        text_fruit = rootView.findViewById(R.id.textView_fruit_recolte);
        text_graine = rootView.findViewById(R.id.textView_graine_extraite);
        text_total_graine = rootView.findViewById(R.id.textView_graine_total);
        TextView text_code_plante = rootView.findViewById(R.id.textView_code_plante);
        ImageView image_qrcode = rootView.findViewById(R.id.imageGeneral);

        // change color icon image
        switch (getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK) {
            case Configuration.UI_MODE_NIGHT_YES:
                image_qrcode.setImageResource(R.drawable.qr_code_white_512_px);
                break;
            case Configuration.UI_MODE_NIGHT_NO:
                image_qrcode.setImageResource(R.drawable.qr_code_black_512_px);
                break;
        }

        String graine_total_plante = "";

        if(plantation.getNombre_plante() == 1){
            graine_total_plante = databaseHelper.getNombreGraineRecolterByCodePlante(plante.getCode_plante()) + "/" + plantation.getGraine_objectif();
        } else if(plantation.getNombre_plante() == 2){
            graine_total_plante = databaseHelper.getNombreGraineRecolterByCodePlante(plante.getCode_plante()) + "/" + plantation.getGraine_objectif() / 2;
        } else {
            graine_total_plante = databaseHelper.getNombreGraineRecolterByCodePlante(plante.getCode_plante()) + "/" + plantation.getGraine_objectif() / (plantation.getNombre_plante() - 1);
        }


        String graine_total_plantation = databaseHelper.getNombreGraineRecolterByCodeLotPlante(plantation.getCode_lot_plante()) + "/" + plantation.getGraine_objectif();

        int maxLength = Math.min(plantation.getCode_accession().length(), 20);
        text_accession.setText(plantation.getCode_accession().substring(0, maxLength));
        text_parcelle.setText(plantation.getParcelle());
        text_num_plante.setText(plante.getNum_plante());
        text_type.setText(plante.getType_fecondation());
        text_fruit.setText(String.valueOf(databaseHelper.getNombreFruitRecolterByCodePlante(plante.getCode_plante())));
        text_graine.setText(graine_total_plante);
        text_total_graine.setText(graine_total_plantation);
        text_code_plante.setText(plante.getCode_plante());

        listePlante = new ArrayList<>();
        listePlante = databaseHelper.getAllPlanteForRecyclerViewByCodeLotPlanteExceptCurrentPlante(plantation.getCode_lot_plante(), plante.getCode_plante());
        RecyclerView recyclerView = rootView.findViewById(R.id.recycler_view_generale);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(rootView.getContext());
        adapter = new RecyclerViewGeneraleAdapter(listePlante);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            plante = (Plante) getArguments().getSerializable("plante");
        }
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        mCallback = (FragmentListener) context;
    }

    @SuppressWarnings("unused")
    public static FragmentGenerale newInstance(){
        return new FragmentGenerale();
    }

    public void updateFragmentElement(){
        if(adapter != null && listePlante != null){
            listePlante = databaseHelper.getAllPlanteForRecyclerViewByCodeLotPlanteExceptCurrentPlante(plantation.getCode_lot_plante(), plante.getCode_plante());
            adapter.updateListe(listePlante);
            text_fruit.setText(String.valueOf(databaseHelper.getNombreFruitRecolterByCodePlante(plante.getCode_plante())));
            String graine_total_plante = "";

            if(plantation.getNombre_plante() == 1){
                graine_total_plante = databaseHelper.getNombreGraineRecolterByCodePlante(plante.getCode_plante()) + "/" + plantation.getGraine_objectif();
            } else if(plantation.getNombre_plante() == 2){
                graine_total_plante = databaseHelper.getNombreGraineRecolterByCodePlante(plante.getCode_plante()) + "/" + plantation.getGraine_objectif() / 2;
            } else {
                graine_total_plante = databaseHelper.getNombreGraineRecolterByCodePlante(plante.getCode_plante()) + "/" + plantation.getGraine_objectif() / (plantation.getNombre_plante() - 1);
            }

            String graine_total_plantation = databaseHelper.getNombreGraineRecolterByCodeLotPlante(plantation.getCode_lot_plante()) + "/" + plantation.getGraine_objectif();
            text_graine.setText(graine_total_plante);
            text_total_graine.setText(graine_total_plantation);
        }
    }
}