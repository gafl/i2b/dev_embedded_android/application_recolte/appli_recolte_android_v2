package com.example.application_recolte_v2.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.application_recolte_v2.activity.BluetoothScannerActivity;
import com.example.application_recolte_v2.activity.FragmentContainer;
import com.example.application_recolte_v2.R;
import com.example.application_recolte_v2.modele.ActionCallback;
import com.example.application_recolte_v2.modele.Graine;
import com.example.application_recolte_v2.modele.Plantation;
import com.example.application_recolte_v2.modele.Plante;
import com.example.application_recolte_v2.modele.Recolte;
import com.example.application_recolte_v2.module.SettingsHelper;
import com.example.application_recolte_v2.module.ZebraPrint;
import com.example.application_recolte_v2.recyclerview.OnItemListener;
import com.example.application_recolte_v2.recyclerview.RecyclerViewGraineAdapter;
import com.example.application_recolte_v2.sqlite.DatabaseHelper;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.example.application_recolte_v2.module.PermissionChecker.requestBluetoothPermission;
import static com.example.application_recolte_v2.module.SweetAlert.errorAlert;
import static com.example.application_recolte_v2.module.SweetAlert.successAlert;


public class FragmentGraine extends Fragment implements ActionCallback, OnItemListener {

    private FragmentListener mCallback;
    private FragmentContainer myContext;
    private DatabaseHelper databaseHelper;
    private Plante plante;
    private List<Graine> listeGraine;
    private Button buttonPrint;
    private RecyclerViewGraineAdapter adapter;
    private TextView textView_aucune_graine;
    private ImageView imageNoGraine;
    private ActivityResultLauncher<Intent> activityResultBluetooth;
    private SweetAlertDialog pDialog;

    public FragmentGraine() { }

    @SuppressWarnings("unused")
    public static FragmentGraine newInstance() {
        return new FragmentGraine();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_graine, container, false);

        databaseHelper = new DatabaseHelper(rootView.getContext());
        plante = mCallback.get_plante_from_activity_container();
        listeGraine = new ArrayList<>();
        listeGraine = databaseHelper.getAllGraineByCodePlante(plante.getCode_plante());

        buttonPrint = rootView.findViewById(R.id.button_imprimer_etiquette_finale);
        textView_aucune_graine = rootView.findViewById(R.id.textView_vide_graine);
        imageNoGraine = rootView.findViewById(R.id.graine_no_extraction);
        RecyclerView recyclerView = rootView.findViewById(R.id.recyclerView_graine);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(rootView.getContext());
        adapter = new RecyclerViewGraineAdapter(listeGraine, this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        updateRecyclerView();
        ChangeVisibilityElementIfListGraineEmpty();

        buttonPrint.setOnClickListener(view -> requestBluetoothPermission(myContext, this));

        // Activity result from bluetooth activity
        activityResultBluetooth = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        assert result.getData() != null;
                        String macAddress = result.getData().getStringExtra("mac");
                        SettingsHelper.saveBluetoothAddress(myContext, macAddress);
                        launchImpression(macAddress);
                    }
                });

        return rootView;
    }

    private void ChangeVisibilityElementIfListGraineEmpty() {
        if(listeGraine.isEmpty()){
            buttonPrint.setVisibility(View.INVISIBLE);
            textView_aucune_graine.setVisibility(View.VISIBLE);
            imageNoGraine.setVisibility(View.VISIBLE);
        } else{
            buttonPrint.setVisibility(View.VISIBLE);
            textView_aucune_graine.setVisibility(View.INVISIBLE);
            imageNoGraine.setVisibility(View.INVISIBLE);
        }
    }

    public void launchImpression(String macAddress){
        pDialog = new SweetAlertDialog(myContext, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(myContext.getResources().getString(R.string.impression_en_cours));
        pDialog.setCancelable(false);
        pDialog.show();
        ZebraPrint zebraPrint = new ZebraPrint(this, myContext);
        Plantation plantation = databaseHelper.getPlantationByCodeLotPlante(plante.getCode_lot_plante());
        Recolte recolte = databaseHelper.getLastRecolteFromPlanteByCodePlante(plante.getCode_plante());
        zebraPrint.print(macAddress, plante, plantation, recolte, true);
    }

    public void updateRecyclerView(){
        if(adapter != null && listeGraine != null){
            adapter.updateListe(databaseHelper.getAllGraineByCodePlante(plante.getCode_plante()));
            ChangeVisibilityElementIfListGraineEmpty();
        }
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        mCallback = (FragmentListener) context;
        myContext = (FragmentContainer) context;
    }

    @Override
    public void action(int code, String message) {
        if(ActionCallback.PERMISSION_BLUETOOTH_GRANTED == code){
            if(SettingsHelper.getBluetoothAddress(myContext).equals("none")) {
                activityResultBluetooth.launch(new Intent(myContext, BluetoothScannerActivity.class));
            } else {
                launchImpression(SettingsHelper.getBluetoothAddress(myContext));
            }
        } else if (ActionCallback.SUCCESS == code){
            pDialog.dismiss();
            successAlert(message, myContext);
        } else if (ActionCallback.ERROR == code){
            pDialog.dismiss();
            errorAlert(message, myContext);
        }
    }

    @Override
    public void onItemClick(int position) {
        Graine graine = listeGraine.get(position);
        ItemGrainDialogFragment itemGrainDialogFragment = newInstance(graine, String.valueOf(position+1));
        itemGrainDialogFragment.show(myContext.getSupportFragmentManager(), "Modifier fragment");

        myContext.getSupportFragmentManager().executePendingTransactions();
        Objects.requireNonNull(itemGrainDialogFragment.getDialog()).setOnDismissListener(dialogInterface -> updateRecyclerView());
    }

    @Override
    public void onLongItemClick(int position) {

    }

    public static ItemGrainDialogFragment newInstance(Graine graine, String num_recolte){
        ItemGrainDialogFragment item = new ItemGrainDialogFragment();

        String code_recolte, code_graine, nombre_graine;

        code_recolte = graine.getCode_recolte();
        code_graine = graine.getCode_graine();
        nombre_graine = String.valueOf(graine.getNombre_graine_extraite());


        Bundle args = new Bundle();
        args.putString("num_recolte", num_recolte);
        args.putString("code_recolte", code_recolte);
        args.putString("code_graine", code_graine);
        args.putString("nombre_graine", nombre_graine);

        item.setArguments(args);

        return item;
    }
}