package com.example.application_recolte_v2.modele;

import java.io.Serializable;

@SuppressWarnings("unused")
public class Plantation implements Serializable {
    private int id;
    private String code_accession;
    private String code_lot_plante;
    private String lieu;
    private String parcelle;
    private String nom_lot;
    private int nombre_plante;
    private String lot_origine;
    private String espece;
    private int graine_objectif;
    private String remarque;
    private String modifier_le;

    public Plantation(int id, String code_accession, String code_lot_plante, String lieu, String parcelle, String nom_lot, int nombre_plante, String lot_origine, String espece, int graine_objectif, String remarque, String modifier_le) {
        this.id = id;
        this.code_accession = code_accession;
        this.code_lot_plante = code_lot_plante;
        this.lieu = lieu;
        this.parcelle = parcelle;
        this.nom_lot = nom_lot;
        this.nombre_plante = nombre_plante;
        this.lot_origine = lot_origine;
        this.espece = espece;
        this.graine_objectif = graine_objectif;
        this.remarque = remarque;
        this.modifier_le = modifier_le;
    }

    public Plantation(String code_accession, String code_lot_plante, String lieu, String parcelle, String nom_lot, int nombre_plante, String lot_origine, String espece, int graine_objectif, String remarque, String modifier_le) {
        this.id = 1;
        this.code_accession = code_accession;
        this.code_lot_plante = code_lot_plante;
        this.lieu = lieu;
        this.parcelle = parcelle;
        this.nom_lot = nom_lot;
        this.nombre_plante = nombre_plante;
        this.lot_origine = lot_origine;
        this.espece = espece;
        this.graine_objectif = graine_objectif;
        this.remarque = remarque;
        this.modifier_le = modifier_le;
    }

    public Plantation(String code_accession, String code_lot_plante, String lieu, String parcelle, String nom_lot, int nombre_plante, String lot_origine, String espece, int graine_objectif, String remarque) {
        this.code_accession = code_accession;
        this.code_lot_plante = code_lot_plante;
        this.lieu = lieu;
        this.parcelle = parcelle;
        this.nom_lot = nom_lot;
        this.nombre_plante = nombre_plante;
        this.lot_origine = lot_origine;
        this.espece = espece;
        this.graine_objectif = graine_objectif;
        this.remarque = remarque;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode_accession() {
        return code_accession;
    }

    public void setCode_accession(String code_accession) {
        this.code_accession = code_accession;
    }

    public String getCode_lot_plante() {
        return code_lot_plante;
    }

    public void setCode_lot_plante(String code_lot_plante) {
        this.code_lot_plante = code_lot_plante;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public String getParcelle() {
        return parcelle;
    }

    public void setParcelle(String parcelle) {
        this.parcelle = parcelle;
    }

    public String getNom_lot() {
        return nom_lot;
    }

    public void setNom_lot(String nom_lot) {
        this.nom_lot = nom_lot;
    }

    public int getNombre_plante() {
        return nombre_plante;
    }

    public void setNombre_plante(int nombre_plante) {
        this.nombre_plante = nombre_plante;
    }

    public String getLot_origine() {
        return lot_origine;
    }

    public void setLot_origine(String lot_origine) {
        this.lot_origine = lot_origine;
    }

    public String getEspece() {
        return espece;
    }

    public void setEspece(String espece) {
        this.espece = espece;
    }

    public int getGraine_objectif() {
        return graine_objectif;
    }

    public void setGraine_objectif(int graine_objectif) {
        this.graine_objectif = graine_objectif;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public String getModifier_le() {
        return modifier_le;
    }

    public void setModifier_le(String modifier_le) {
        this.modifier_le = modifier_le;
    }
}
