package com.example.application_recolte_v2.modele;

import java.io.Serializable;

public class Graine implements Serializable {
    private String code_recolte;
    private String code_graine;
    private int nombre_graine_extraite;
    private String modifier_le;

    public Graine(String code_recolte, String code_graine, int nombre_graine_extraite, String modifier_le) {
        this.code_recolte = code_recolte;
        this.code_graine = code_graine;
        this.nombre_graine_extraite = nombre_graine_extraite;
        this.modifier_le = modifier_le;
    }

    public String getCode_recolte() {
        return code_recolte;
    }

    public void setCode_recolte(String code_recolte) {
        this.code_recolte = code_recolte;
    }

    public String getCode_graine() {
        return code_graine;
    }

    public void setCode_graine(String code_graine) {
        this.code_graine = code_graine;
    }

    public int getNombre_graine_extraite() {
        return nombre_graine_extraite;
    }

    @SuppressWarnings("unused")
    public void setNombre_graine_extraite(int nombre_graine_extraite) {
        this.nombre_graine_extraite = nombre_graine_extraite;
    }

    public String getModifier_le() {
        return modifier_le;
    }

    public void setModifier_le(String modifier_le) {
        this.modifier_le = modifier_le;
    }
}
