package com.example.application_recolte_v2.modele;

import java.io.Serializable;

@SuppressWarnings("unused")
public class Recolte implements Serializable {
    private String code_plante;
    private String code_recolte;
    private int num_recolte;
    private int nombre_fruit_recolter;
    private String modifier_le;

    public Recolte(String code_plante, String code_recolte, int num_recolte, int nombre_fruit_recolter, String modifier_le) {
        this.code_plante = code_plante;
        this.code_recolte = code_recolte;
        this.num_recolte = num_recolte;
        this.nombre_fruit_recolter = nombre_fruit_recolter;
        this.modifier_le = modifier_le;
    }

    public String getCode_plante() {
        return code_plante;
    }

    public void setCode_plante(String code_plante) {
        this.code_plante = code_plante;
    }

    public String getCode_recolte() {
        return code_recolte;
    }

    public void setCode_recolte(String code_recolte) {
        this.code_recolte = code_recolte;
    }

    public int getNum_recolte() {
        return num_recolte;
    }

    public void setNum_recolte(int num_recolte) {
        this.num_recolte = num_recolte;
    }

    public int getNombre_fruit_recolter() {
        return nombre_fruit_recolter;
    }

    public void setNombre_fruit_recolter(int nombre_fruit_recolter) {
        this.nombre_fruit_recolter = nombre_fruit_recolter;
    }

    public String getModifier_le() {
        return modifier_le;
    }

    public void setModifier_le(String modifier_le) {
        this.modifier_le = modifier_le;
    }
}
