package com.example.application_recolte_v2.modele;

public interface ActionCallback {
    int ERROR = -1;
    int SUCCESS = 0;
    int PERMISSION_CAMERA_GRANTED = 1;
    int PERMISSION_READ_GRANTED = 2;
    int PERMISSION_BLUETOOTH_GRANTED = 41;
    int GET_PLANTATION = 10;
    int GET_PLANTE = 11;
    int SEND_PLANTE = 12;
    int GET_RECOLTE = 13;
    int SEND_RECOLTE = 14;
    int GET_GRAINE = 15;
    int SEND_GRAINE = 16;
    int SUCCESS_DISCONNECTION = 20 ;
    void action(int code, String message);
}
