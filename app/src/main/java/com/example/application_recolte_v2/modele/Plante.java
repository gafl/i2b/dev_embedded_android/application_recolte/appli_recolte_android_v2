package com.example.application_recolte_v2.modele;

import java.io.Serializable;

@SuppressWarnings("unused")
public class Plante implements Serializable {

    private int id;
    private String code_lot_plante;
    private String num_plante;
    private String type_fecondation;
    private String code_plante;
    private String modifier_le;
    private int nombre_graine_extraite;

    public Plante(String code_lot_plante, String num_plante, String type_fecondation, String code_plante, String modifier_le, int nombre_graine_extraite) {
        this.code_lot_plante = code_lot_plante;
        this.num_plante = num_plante;
        this.type_fecondation = type_fecondation;
        this.code_plante = code_plante;
        this.modifier_le = modifier_le;
        this.nombre_graine_extraite = nombre_graine_extraite;
    }

    public Plante(String code_lot_plante, String num_plante, String type_fecondation, String code_plante, String modifier_le) {
        this.code_lot_plante = code_lot_plante;
        this.num_plante = num_plante;
        this.type_fecondation = type_fecondation;
        this.code_plante = code_plante;
        this.modifier_le = modifier_le;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode_lot_plante() {
        return code_lot_plante;
    }

    public void setCode_lot_plante(String code_lot_plante) {
        this.code_lot_plante = code_lot_plante;
    }

    public String getNum_plante() {
        return num_plante;
    }

    public void setNum_plante(String num_plante) {
        this.num_plante = num_plante;
    }

    public String getType_fecondation() {
        return type_fecondation;
    }

    public void setType_fecondation(String type_fecondation) {
        this.type_fecondation = type_fecondation;
    }

    public String getCode_plante() {
        return code_plante;
    }

    public void setCode_plante(String code_plante) {
        this.code_plante = code_plante;
    }

    public String getModifier_le() {
        return modifier_le;
    }

    public void setModifier_le(String modifier_le) {
        this.modifier_le = modifier_le;
    }

    public int getNombre_graine_extraite() {
        return nombre_graine_extraite;
    }

    public void setNombre_graine_extraite(int nombre_graine_extraite) {
        this.nombre_graine_extraite = nombre_graine_extraite;
    }
}
