package com.example.application_recolte_v2.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager2.widget.ViewPager2;

import com.example.application_recolte_v2.R;
import com.example.application_recolte_v2.fragment.FragmentFruit;
import com.example.application_recolte_v2.fragment.FragmentGenerale;
import com.example.application_recolte_v2.fragment.FragmentGraine;
import com.example.application_recolte_v2.fragment.FragmentListener;
import com.example.application_recolte_v2.fragment.ViewPageAdapter;
import com.example.application_recolte_v2.modele.Plantation;
import com.example.application_recolte_v2.modele.Plante;
import com.example.application_recolte_v2.sqlite.DatabaseHelper;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.Objects;

public class FragmentContainer extends AppCompatActivity implements FragmentListener {

    private Plante plante;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_container);

        // Toolbar settings
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("");
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> finish());

        // Initialisation
        TabLayout tabLayout = findViewById(R.id.tabLayout);
        ViewPager2 viewPager = findViewById(R.id.viewPager_container);

        plante = (Plante) getIntent().getSerializableExtra("plante");

        ViewPageAdapter adapter = new ViewPageAdapter(this);
        viewPager.setAdapter(adapter);

        new TabLayoutMediator(tabLayout, viewPager, (tab, position) -> {
            switch(position){
                case 0:
                    tab.setText(R.string.tab_general);
                    break;
                case 1:
                    tab.setText(R.string.tab_fruit);
                    break;
                default:
                    tab.setText(R.string.tab_graine);
            }
        }).attach();

        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {

            @Override
            public void onPageSelected(int position) {
                if(position == 0){
                    FragmentGenerale fragment = (FragmentGenerale) getSupportFragmentManager().findFragmentByTag("f" + 0);
                    if(fragment != null){
                        fragment.updateFragmentElement();
                    }
                }
                else if(position == 1) {
                    FragmentFruit fragment = (FragmentFruit) getSupportFragmentManager().findFragmentByTag("f" + 1);
                    if(fragment != null){
                        fragment.updateRecyclerView();
                    }
                } else {
                    FragmentGraine fragment = (FragmentGraine) getSupportFragmentManager().findFragmentByTag("f" + 2);
                    if(fragment != null){
                        fragment.updateRecyclerView();
                    }
                }
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { super.onPageScrolled(position, positionOffset, positionOffsetPixels); }

            @Override
            public void onPageScrollStateChanged(int state) { super.onPageScrollStateChanged(state); }
        });
    }

    @Override
    public Plantation get_plantation_from_activity_container() {
        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        return databaseHelper.getPlantationByCodeLotPlante(plante.getCode_lot_plante());
    }

    @Override
    public Plante get_plante_from_activity_container() {
        return plante;
    }

}