package com.example.application_recolte_v2.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceManager;

import com.example.application_recolte_v2.R;
import com.example.application_recolte_v2.module.StatusBarManager;
import com.example.application_recolte_v2.fragment.ParametreFragment;

import java.util.Locale;
import java.util.Objects;

import static com.example.application_recolte_v2.module.Utils.getSettingLanguage;
import static com.example.application_recolte_v2.module.Utils.setAppLanguage;

public class ParametreActivity extends AppCompatActivity implements ParametreFragment.OnDataPass{

    private static int requestCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.icon_background_dark_and_light);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parametre);

        ParametreFragment parametreFragment = new ParametreFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.container_parametre, parametreFragment).commit();

        if(!getResources().getConfiguration().getLocales().get(0).getCountry().equals(getSettingLanguage(this))){
            setAppLanguage(this);
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(getResources().getString(R.string.parametre));
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        StatusBarManager.testIfDataNeedToBeSynchronize(findViewById(R.id.include_status_bar), this);

    }

    @Override
    public void onBackPressed() {
        setResult(requestCode);
        overridePendingTransition(0, 0);
        super.onBackPressed();
    }

    @Override
    public void onDataPass(int code) {
        requestCode = code;
    }

    // This override method prevent Dark mode to change locale language
    // https://stackoverflow.com/questions/64168632/appcompatdelegate-mode-night-follow-system-not-working
    @Override
    protected void attachBaseContext(Context newBase) {

        Resources resources = newBase.getResources();
        Configuration configuration = new Configuration(resources.getConfiguration());
        configuration.uiMode = Configuration.UI_MODE_NIGHT_UNDEFINED;
        Context context = newBase.createConfigurationContext(configuration);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String langue = sharedPreferences.getString("langage_pref", "fr");
        Locale locale = new Locale(langue);
        Locale.setDefault(locale);
        configuration.setLocale(locale);
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());

        super.attachBaseContext(newBase);
    }

}