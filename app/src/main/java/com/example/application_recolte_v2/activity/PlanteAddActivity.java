package com.example.application_recolte_v2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.application_recolte_v2.R;
import com.example.application_recolte_v2.module.StatusBarManager;
import com.example.application_recolte_v2.modele.Plantation;
import com.example.application_recolte_v2.modele.Plante;
import com.example.application_recolte_v2.module.SweetAlert;
import com.example.application_recolte_v2.recyclerview.OnItemListener;
import com.example.application_recolte_v2.recyclerview.RecyclerViewPlanteAdapter;
import com.example.application_recolte_v2.sqlite.DatabaseHelper;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.example.application_recolte_v2.module.SettingsHelper.databaseHasChanged;
import static com.example.application_recolte_v2.module.SweetAlert.choiceAlert;
import static com.example.application_recolte_v2.module.SweetAlert.errorAlert;
import static com.example.application_recolte_v2.sqlite.PlanteTableHelper.rowPlanteExistByCodePlanteHelper;

public class PlanteAddActivity extends AppCompatActivity implements SweetAlert.CallbackSweetAlert, OnItemListener {

    private DatabaseHelper databaseHelper;
    private Plantation plantation;
    private Spinner spinner_num, spinner_fecondation;
    private List<Plante> listePlante;
    private Button button_valider;
    private TextView textView_no_plante;
    private ImageView imageView_no_plante;
    private Plante planteItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plante_add);

        // Toolbar settings
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(getResources().getString(R.string.parametre));
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> finish());
        StatusBarManager.testIfDataNeedToBeSynchronize(findViewById(R.id.include_status_bar), this);

        // Initialization parameter
        databaseHelper = new DatabaseHelper(this);
        TextView textView_qrcode_scan = findViewById(R.id.textView_Qrcode_scan);
        spinner_num = findViewById(R.id.spinner_num_plante);
        spinner_fecondation = findViewById(R.id.spinner_fecondation);
        button_valider = findViewById(R.id.button_add_plante);
        RecyclerView recyclerView = findViewById(R.id.recycler_view_autre_plante);
        imageView_no_plante = findViewById(R.id.image_plante_add);
        textView_no_plante = findViewById(R.id.textView_plante_not_found);

        // Get plantation from intent extra
        plantation = (Plantation) getIntent().getSerializableExtra("plantation");

        // Set qrcode textview with plantation code_lot_plante
        textView_qrcode_scan.setText(plantation.getCode_lot_plante());

        button_valider.setOnClickListener(v -> choiceAlert(getResources().getString(R.string.etes_vous_sur), getResources().getString(R.string.ajouter_plante), this, this,0));

        addPopulationAndListenerToSpinner();

        // Initialization of recyclerview
        listePlante = new ArrayList<>();
        listePlante = databaseHelper.getAllPlanteFromPlantationByCodeLotPlante(plantation.getCode_lot_plante());

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        RecyclerViewPlanteAdapter adapter = new RecyclerViewPlanteAdapter(listePlante, this, this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        ChangeVisibilityElementIfListPlanteEmpty();
    }

    private void ChangeVisibilityElementIfListPlanteEmpty() {
        if(listePlante.isEmpty()){
            textView_no_plante.setVisibility(View.VISIBLE);
            imageView_no_plante.setVisibility(View.VISIBLE);
        } else{
            textView_no_plante.setVisibility(View.INVISIBLE);
            imageView_no_plante.setVisibility(View.INVISIBLE);
        }
    }

    private void addPopulationAndListenerToSpinner() {
        // Populate List
        List<String> spinnerArrayNumPlante = populateNumPlante();
        List<String> spinnerArrayTypeFecondation = populateTypeFecondation();

        ArrayAdapter<String> adapterSpinnerNumPlante = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, spinnerArrayNumPlante);
        adapterSpinnerNumPlante.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter<String> adapterSpinnerFecondation = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, spinnerArrayTypeFecondation);
        adapterSpinnerFecondation.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner_num.setAdapter(adapterSpinnerNumPlante);
        spinner_fecondation.setAdapter(adapterSpinnerFecondation);
        spinner_fecondation.setSelection(1); // Get fecondation B by default


        spinner_num.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String code_plante = getCodePlanteGenerate();
                String button_text = getResources().getString(R.string.valider) + " " + code_plante;
                button_valider.setText(button_text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) { }
        });

        spinner_fecondation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String code_plante = getCodePlanteGenerate();
                String button_text = getResources().getString(R.string.valider) + " " + code_plante;
                button_valider.setText(button_text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) { }
        });
    }

    private String getCodePlanteGenerate() {
        String code_lot_plante = plantation.getCode_lot_plante();
        String num_plante = spinner_num.getSelectedItem().toString();
        String type_fecondation = spinner_fecondation.getSelectedItem().toString();

        return code_lot_plante + num_plante + type_fecondation;
    }

    private List<String> populateNumPlante() {
        List<String> spinnerArray = new ArrayList<>();

        int max_plante = plantation.getNombre_plante();

        for(int i = 1; i <= max_plante; i++){
            spinnerArray.add(String.valueOf(i));
        }

        spinnerArray.add("*");

        List<Plante> listePlante = databaseHelper.getAllPlanteFromPlantationByCodeLotPlante(plantation.getCode_lot_plante());

        //noinspection ConstantConditions
        if(listePlante != null || listePlante.size() != 0){
            for(Plante plante : listePlante){
                if(!plante.getNum_plante().equals("*")){
                    spinnerArray.remove(plante.getNum_plante());
                }
            }
        }

        return spinnerArray;
    }

    private List<String> populateTypeFecondation() {
        List<String> spinnerArray = new ArrayList<>();

        spinnerArray.add("A");
        spinnerArray.add("B");
        spinnerArray.add("C");
        spinnerArray.add("D");
        spinnerArray.add("E");
        spinnerArray.add("F");

        return spinnerArray;
    }

    @Override
    public void onActionSweetAlert(boolean bool, int request) {
        if(bool && request == 0){
            String code_plante = getCodePlanteGenerate();
            String currentDate = OffsetDateTime.now(ZoneId.of("Europe/Paris")).toString();

            Plante plante = new Plante(plantation.getCode_lot_plante(), spinner_num.getSelectedItem().toString(), spinner_fecondation.getSelectedItem().toString(), code_plante, currentDate);

            if(!rowPlanteExistByCodePlanteHelper(databaseHelper, plante.getCode_plante())){
                databaseHelper.insertOnPlante(plante);
                databaseHasChanged(this);
                Intent intent = new Intent(PlanteAddActivity.this, FragmentContainer.class);
                intent.putExtra("plante", plante);
                startActivity(intent);
                finish();
            } else {
                errorAlert(getString(R.string.la_plante_label) + " " + plante.getCode_plante() + " " + getString(R.string.existe_deja), this);
            }
        } else if (bool && request == 1){
            Intent intent = new Intent(PlanteAddActivity.this, FragmentContainer.class);
            intent.putExtra("plante", planteItem);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onItemClick(int position) {
        planteItem = listePlante.get(position);
        choiceAlert(getResources().getString(R.string.etes_vous_sur), getString(R.string.aller_sur_plante) + " " + planteItem.getCode_plante(), this, this, 1);
    }

    @Override
    public void onLongItemClick(int position) {

    }
}