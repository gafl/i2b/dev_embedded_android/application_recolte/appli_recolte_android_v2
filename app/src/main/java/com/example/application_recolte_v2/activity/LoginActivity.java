package com.example.application_recolte_v2.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BlendMode;
import android.graphics.BlendModeColorFilter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.ColorInt;
import androidx.appcompat.app.AppCompatActivity;

import com.example.application_recolte_v2.R;
import com.example.application_recolte_v2.modele.ActionCallback;
import com.example.application_recolte_v2.module.SettingsHelper;
import com.example.application_recolte_v2.module.SweetAlert;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.example.application_recolte_v2.module.SettingsHelper.saveToken;
import static com.example.application_recolte_v2.module.SweetAlert.choiceAlert;
import static com.example.application_recolte_v2.module.SweetAlert.errorAlert;
import static com.example.application_recolte_v2.module.Utils.hideKeyboard;
import static com.example.application_recolte_v2.module.Utils.restartActivity;
import static com.example.application_recolte_v2.module.Utils.setAppLanguage;
import static com.example.application_recolte_v2.request.RequestUser.loginToServer;
import static com.example.application_recolte_v2.request.RequestUtil.WRONG_SERVER_SETTINGS;

public class LoginActivity extends AppCompatActivity implements ActionCallback, SweetAlert.CallbackSweetAlert {

    private EditText username, password;
    private SweetAlertDialog pDialog;
    private Button login_button;
    private View dividerUsername, dividerPassword;
    private ImageView close_imageView;
    private ActivityResultLauncher<Intent> activityResultParametre;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        /*
         * Set status bar and navigation bar color
         * Get the color from background and apply on them
         * Now status and nav bar have the same color than splashscreen background
         * */
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = this.getTheme();
        theme.resolveAttribute(R.attr.background, typedValue, true);
        @ColorInt int color = typedValue.data;
        getWindow().setNavigationBarColor(color);
        getWindow().setStatusBarColor(color);

        close_imageView = findViewById(R.id.close_imageview_login);
        username = findViewById(R.id.edittext_username);
        password = findViewById(R.id.editText_password);
        login_button = findViewById(R.id.button_login);
        dividerUsername = findViewById(R.id.dividerUsername);
        dividerPassword = findViewById(R.id.dividerPassword);

        // Set username & password with encrypted preference shared
        // This take to much time so I used thread
        new Thread(() -> {
            String usernameStr = SettingsHelper.getUsername(this);
            String passwordStr = SettingsHelper.getPassword(this);

            this.runOnUiThread(() -> {
                username.setText(usernameStr);
                password.setText(passwordStr);
            });
        }).start();


        enableButtonlogin(false);

        tryEnableButtonLogin();

        setAllListener();
    }

    private void setAllListener() {
        close_imageView.setOnClickListener(v -> finish());

        login_button.setOnClickListener(v -> {
            hideKeyboard(this);
            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText(getResources().getString(R.string.connexion_en_cours) + "...");
            pDialog.setCancelable(false);
            pDialog.show();

            new Thread(() -> loginToServer(this, this, username.getText().toString(), password.getText().toString())).start();
        });

        username.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @SuppressWarnings({"deprecation", "RedundantSuppression"})
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(username.getText().length() < 4 && username.getText().length() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        dividerUsername.getBackground().setColorFilter(new BlendModeColorFilter(getResources().getColor(R.color.red, getTheme()), BlendMode.SRC_ATOP));
                    } else {
                        dividerUsername.getBackground().setColorFilter(getResources().getColor(R.color.red, getTheme()), PorterDuff.Mode.SRC_ATOP);
                    }
                } else {
                    dividerUsername.getBackground().clearColorFilter();
                }

                tryEnableButtonLogin();
            }
        });

        password.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @SuppressWarnings({"deprecation", "RedundantSuppression"})
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(password.getText().length() < 8 && password.getText().length() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        dividerPassword.getBackground().setColorFilter(new BlendModeColorFilter(getResources().getColor(R.color.red, getTheme()), BlendMode.SRC_ATOP));
                    } else {
                        dividerPassword.getBackground().setColorFilter(getResources().getColor(R.color.red, getTheme()), PorterDuff.Mode.SRC_ATOP);
                    }
                } else {
                    dividerPassword.getBackground().clearColorFilter();
                }

                tryEnableButtonLogin();
            }
        });

        // Activity result from parametre activity
        activityResultParametre = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == SUCCESS) {
                        setAppLanguage(this);
                        restartActivity(this);
                    }
                });
    }

    private void tryEnableButtonLogin(){
        enableButtonlogin(username.getText().length() >= 4 && password.getText().length() >= 8);
    }

    @SuppressWarnings({"deprecation", "RedundantSuppression"})
    private void enableButtonlogin(boolean enable){
        if(enable){
            login_button.setEnabled(true);
            login_button.getBackground().clearColorFilter();
        } else {
            login_button.setEnabled(false);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                login_button.getBackground().setColorFilter(new BlendModeColorFilter(getResources().getColor(R.color.disable, getTheme()), BlendMode.SRC_ATOP));
            } else {
                login_button.getBackground().setColorFilter(getResources().getColor(R.color.disable, getTheme()), PorterDuff.Mode.SRC_ATOP);
            }
        }
    }

    @Override
    public void action(int action, String message) {
        switch(action) {
            case SUCCESS:
                this.runOnUiThread(() -> {

                    new Thread(() -> {
                        saveToken(this, message);
                        SettingsHelper.storeEncryptedUsernameAndPassword(this, username.getText().toString(), password.getText().toString());

                        this.runOnUiThread(() -> {
                            pDialog.dismiss();
                            setResult(RESULT_OK);
                            finish();
                        });
                    }).start();
                });
                break;
            case ERROR:
                this.runOnUiThread(() -> {
                    pDialog.dismiss();
                    password.setText("");
                    errorAlert(message, this);
                });
                break;
            case WRONG_SERVER_SETTINGS:
                this.runOnUiThread(() -> {
                    pDialog.dismiss();
                    username.setText("");
                    password.setText("");
                    choiceAlert(getResources().getString(R.string.probleme), message, this, this, WRONG_SERVER_SETTINGS);
                });
                break;
        }
    }

    @Override
    public void onActionSweetAlert(boolean bool, int request) {
        if(bool && request == WRONG_SERVER_SETTINGS){
            activityResultParametre.launch(new Intent(LoginActivity.this, ParametreActivity.class));
            overridePendingTransition(0, 0);
        } else {
            finish();
        }
    }
}