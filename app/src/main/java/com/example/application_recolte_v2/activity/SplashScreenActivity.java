package com.example.application_recolte_v2.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.TypedValue;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.preference.PreferenceManager;

import com.example.application_recolte_v2.MainActivity;
import com.example.application_recolte_v2.R;

import static com.example.application_recolte_v2.module.Utils.setAppLanguage;

public class SplashScreenActivity extends AppCompatActivity {

    private final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        /*
        * Get the dark mode preference used on setting
        * Then enable or disable dark theme
        * */
        PreferenceManager.setDefaultValues(this, R.xml.parametres, false);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean isDarkModeActive = sharedPreferences.getBoolean("mode_sombre_pref", false);

        if(!isDarkModeActive){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        } else{
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }

        // Update language app
        setAppLanguage(this);


        TextView text_version = findViewById(R.id.text_version_splash_screen);
        String version = context.getString(R.string.version) + " " + context.getString(R.string.version_number);
        text_version.setText(version);

        /*
        * Set status bar and navigation bar color
        * Get the color from background and apply on them
        * Now status and nav bar have the same color than splashscreen background
        * */
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = context.getTheme();
        theme.resolveAttribute(R.attr.background, typedValue, true);
        @ColorInt int color = typedValue.data;
        getWindow().setNavigationBarColor(color);
        getWindow().setStatusBarColor(color);

        /*
        * After a short delay launch Main Activity and Finish SplashScreen
        * */
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(() -> {
            Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }, 1500);

    }
}