package com.example.application_recolte_v2.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import com.example.application_recolte_v2.R;
import com.example.application_recolte_v2.module.SweetAlert;

import org.w3c.dom.Text;

import java.io.IOException;

public class BluetoothScannerActivity extends AppCompatActivity implements SweetAlert.CallbackSweetAlert {

    private TextView textView;
    private BluetoothAdapter bluetoothAdapter;
    private ArrayAdapter<String> listAdapter;
    private ActivityResultLauncher<Intent> activityResultEnableBluetooth, activityResultEnableLocation;
    private TextView text_printer_not_found;
    private ImageView image_printer_not_found;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_scanner);

        // Initialization
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        ListView devicesList = findViewById(R.id.listviewDeviceBluetooth);
        Button buttonCancel = findViewById(R.id.buttonCancelBluetooth);
        textView = findViewById(R.id.textActionBluetooth);
        text_printer_not_found = findViewById(R.id.text_no_found_printer);
        image_printer_not_found = findViewById(R.id.image_no_found_printer);

        text_printer_not_found.setVisibility(View.INVISIBLE);
        image_printer_not_found.setVisibility(View.INVISIBLE);

        // Initialization of the ArrayAdapter that will store the detected Bluetooth devices
        listAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
        devicesList.setAdapter(listAdapter);

        // Launch discovery
        launchDiscovery();

        devicesList.setOnItemClickListener((parent, view, position, id) -> {
            String device = (String) parent.getItemAtPosition(position);
            String macAddress = device.substring(device.lastIndexOf("\n") + 1);
            Intent returnIntent = new Intent();
            returnIntent.putExtra("mac", macAddress);
            setResult(RESULT_OK, returnIntent);
            finish();
        });

        buttonCancel.setOnClickListener(view -> {
            bluetoothAdapter.cancelDiscovery();
            listAdapter.clear();
            finish();
        });

        activityResultEnableBluetooth = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        launchDiscovery();
                    } else {
                        finish();
                    }
                });

        activityResultEnableLocation = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if(isLocationEnabled(this)){
                        launchDiscovery();
                    } else {
                        finish();
                    }
                });

    }

    private void launchDiscovery() {
        registerReceiver(devicesFoundReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
        registerReceiver(devicesFoundReceiver, new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED));
        registerReceiver(devicesFoundReceiver, new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED));

        new Handler(Looper.getMainLooper()).postDelayed(() -> {
            if(bluetoothAdapter != null && bluetoothAdapter.isEnabled() && isLocationEnabled(this)) {
                listAdapter.clear();
                bluetoothAdapter.startDiscovery();
            } else {
                checkBluetoothState();
            }
        }, 1500);
    }

    private void checkBluetoothState() {
        if(bluetoothAdapter.isEnabled()){
            if(bluetoothAdapter.isDiscovering()){
                textView.setText(getResources().getString(R.string.recherche_peripherique_bluetooth));
            } else {
                textView.setText(R.string.scan_va_commencer);
            }
        } else {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activityResultEnableBluetooth.launch(enableIntent);
        }

        if(!isLocationEnabled(this) && bluetoothAdapter.isEnabled()){
            SweetAlert.choiceAlert(getString(R.string.probleme), getString(R.string.enable_location), this, this, 0);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(devicesFoundReceiver);
    }

    private final BroadcastReceiver devicesFoundReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (device.getAddress().contains(getString(R.string.zebra_printer_signature))) {
                    boolean alreadyAdded = false;

                    for(int i = 0; i < listAdapter.getCount(); i++){
                        String mac = listAdapter.getItem(i).substring(listAdapter.getItem(i).lastIndexOf("\n") + 1);
                        if(mac.equals(device.getAddress())){
                            alreadyAdded = true;
                            break;
                        }
                    }

                    if(!alreadyAdded){
                        listAdapter.add(device.getName() + "\n" + device.getAddress());
                        listAdapter.notifyDataSetChanged();
                    }
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                textView.setText(getResources().getString(R.string.recherche_peripherique_bluetooth));
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                textView.setText(getResources().getString(R.string.recherche_bluetooth_terminer));
                if (listAdapter.isEmpty()) {
                    text_printer_not_found.setVisibility(View.VISIBLE);
                    image_printer_not_found.setVisibility(View.VISIBLE);
                }
            }
        }
    };

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;

        try {
            locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return locationMode != Settings.Secure.LOCATION_MODE_OFF;
    }

    @Override
    public void onActionSweetAlert(boolean bool, int request) {
        if(request == 0){
            if(bool){
                activityResultEnableLocation.launch(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            } else {
                finish();
            }
        }
    }
}